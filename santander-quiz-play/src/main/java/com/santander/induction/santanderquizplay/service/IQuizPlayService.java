package com.santander.induction.santanderquizplay.service;

import java.util.List;
import java.util.UUID;

import com.santander.induction.santanderquizplay.model.QuizSubmission;

public interface IQuizPlayService {
	
	QuizSubmission getQuizSubmissionById(UUID submissionId);
//	QuizSubmission createQuizSubmissionId();
	List<QuizSubmission> getAllQuizSubmissions();
}
