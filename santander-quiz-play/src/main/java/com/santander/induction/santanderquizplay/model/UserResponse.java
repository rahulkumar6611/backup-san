package com.santander.induction.santanderquizplay.model;

import java.util.UUID;

public class UserResponse {
	
	private UUID questionId;
	private String domain;
	public enum QuestionResponse {
		CORRECT,
		INCORRECT,
		SKIPPED	
	}
	private QuestionResponse questionResponse;
	
	
	
	public UserResponse() {
		super();
	}



	public UserResponse(UUID questionId, String domain, QuestionResponse questionResponse) {
		super();
		this.questionId = questionId;
		this.domain = domain;
		this.questionResponse = questionResponse;
	}



	public UUID getQuestionId() {
		return questionId;
	}



	public void setQuestionId(UUID questionId) {
		this.questionId = questionId;
	}



	public String getDomain() {
		return domain;
	}



	public void setDomain(String domain) {
		this.domain = domain;
	}



	public QuestionResponse getQuestionResponse() {
		return questionResponse;
	}



	public void setQuestionResponse(QuestionResponse questionResponse) {
		this.questionResponse = questionResponse;
	}



	@Override
	public String toString() {
		return "UserResponse [questionId=" + questionId + ", domain=" + domain + ", questionResponse="
				+ questionResponse + "]";
	}
	
		
	

}
