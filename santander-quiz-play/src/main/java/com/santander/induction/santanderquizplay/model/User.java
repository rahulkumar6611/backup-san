package com.santander.induction.santanderquizplay.model;


public class User {

	private String psid;
	private String email;
	private String password;
	public enum UserRole {
		USER,
		ADMIN
	}
	private UserRole userRole;
	private boolean userActive = false;
	
	
	public User() {
		super();
	}


	public User(String psid, String email, String password, UserRole userRole, boolean userActive) {
		super();
		this.psid = psid;
		this.email = email;
		this.password = password;
		this.userRole = userRole;
		this.userActive = userActive;
	}


	@Override
	public String toString() {
		return "User [psid=" + psid + ", email=" + email + ", password=" + password + ", userRole=" + userRole
				+ ", userActive=" + userActive + "]";
	}


	public String getPsid() {
		return psid;
	}


	public void setPsid(String psid) {
		this.psid = psid;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public UserRole getUserRole() {
		return userRole;
	}


	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}


	public boolean isUserActive() {
		return userActive;
	}


	public void setUserActive(boolean userActive) {
		this.userActive = userActive;
	}
	
}
