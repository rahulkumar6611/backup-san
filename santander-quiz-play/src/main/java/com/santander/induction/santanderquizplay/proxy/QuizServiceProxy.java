package com.santander.induction.santanderquizplay.proxy;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.santander.induction.santanderquizplay.model.Quiz;



@FeignClient(name="santander-quiz-inventory")
public interface QuizServiceProxy {
	
	
	@GetMapping("/quizzes/{quizCode}")
	Quiz findQuizByQuizCode(@PathVariable("quizCode") String quizCode) ;
	
}
