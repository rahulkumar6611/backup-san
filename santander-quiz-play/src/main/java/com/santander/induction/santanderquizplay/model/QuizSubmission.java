package com.santander.induction.santanderquizplay.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "quizSubmissions")
public class QuizSubmission {
	
		@Id
		private UUID submissionId;
	    //@Indexed(unique=true)
	    private String psid;
	    //@Indexed(unique=true)
	    private String quizCode;
	    private int totalQuestions;
	    private ArrayList<UserResponse> userResponses;
	    private int correctlyAnsweredQuestions;
	    private int incorrectAnsweredQuestions;
	    private int unattemptedQuestions;
	    private float totalPointsScored;
	    @CreatedDate
	    private Date quizStartTime=new Date();
	    private Date quizEndTime;
	    @CreatedDate
	    private Date createdOn = new Date();
	    boolean quizStatus = false;

		public QuizSubmission() {
			super();
			// TODO Auto-generated constructor stub
		}


		public QuizSubmission(String psid, String quizCode, int totalQuestions, ArrayList<UserResponse> userResponses,
				int correctlyAnsweredQuestions, int incorrectAnsweredQuestions, int unattemptedQuestions,
				float totalPointsScored, Date quizStartTime, Date quizEndTime, Date createdOn, boolean quizStatus) {
			super();
			this.psid = psid;
			this.quizCode = quizCode;
			this.totalQuestions = totalQuestions;
			this.userResponses = userResponses;
			this.correctlyAnsweredQuestions = correctlyAnsweredQuestions;
			this.incorrectAnsweredQuestions = incorrectAnsweredQuestions;
			this.unattemptedQuestions = unattemptedQuestions;
			this.totalPointsScored = totalPointsScored;
			this.quizStartTime = quizStartTime;
			this.quizEndTime = quizEndTime;
			this.createdOn = createdOn;
			this.quizStatus = quizStatus;
		}



		public UUID getSubmissionId() {	
			return submissionId;
		}

		public void setSubmissionId(UUID submissionId) {
			this.submissionId = submissionId;
		}

		public String getPsid() {
			return psid;
		}

		public void setPsid(String psid) {
			this.psid = psid;
		}

		public String getQuizCode() {
			return quizCode;
		}

		public void setQuizCode(String quizCode) {
			this.quizCode = quizCode;
		}

		public int getTotalQuestions() {
			return totalQuestions;
		}

		public void setTotalQuestions(int totalQuestions) {
			this.totalQuestions = totalQuestions;
		}

		public int getCorrectlyAnsweredQuestions() {
			return correctlyAnsweredQuestions;
		}

		public void setCorrectlyAnsweredQuestions(int correctlyAnsweredQuestions) {
			this.correctlyAnsweredQuestions = correctlyAnsweredQuestions;
		}

		public int getIncorrectAnsweredQuestions() {
			return incorrectAnsweredQuestions;
		}

		public void setIncorrectAnsweredQuestions(int incorrectAnsweredQuestions) {
			this.incorrectAnsweredQuestions = incorrectAnsweredQuestions;
		}

		public int getUnattemptedQuestions() {
			return unattemptedQuestions;
		}

		public void setUnattemptedQuestions(int unattemptedQuestions) {
			this.unattemptedQuestions = unattemptedQuestions;
		}

		public float getTotalPointsScored() {
			return totalPointsScored;
		}

		public void setTotalPointsScored(float totalPointsScored) {
			this.totalPointsScored = totalPointsScored;
		}

		public Date getQuizStartTime() {
			return quizStartTime;
		}

		public void setQuizStartTime(Date quizStartTime) {
			this.quizStartTime = quizStartTime;
		}

		public Date getQuizEndTime() {
			return quizEndTime;
		}

		public void setQuizEndTime(Date quizEndTime) {
			this.quizEndTime = quizEndTime;
		}

		public Date getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}



		public boolean isQuizStatus() {
			return quizStatus;
		}

		public void setQuizStatus(boolean quizStatus) {
			this.quizStatus = quizStatus;
		}


		public ArrayList<UserResponse> getUserResponses() {
			return userResponses;
		}


		public void setUserResponses(ArrayList<UserResponse> userResponses) {
			this.userResponses = userResponses;
		}
	    
		
		
	    

}
