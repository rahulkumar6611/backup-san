package com.santander.induction.santanderquizplay.proxy;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.santander.induction.santanderquizplay.model.User;


@FeignClient(name="santander-user-management")
public interface UserServiceProxy {

	@GetMapping("users/{psid}")
	User findByUserPsid(@PathVariable String psid);
	
	
	@PostMapping("/users")
	public User addNewUser(@RequestBody User user);
	
	
	@GetMapping("/mail")
	public void sendEmail();
	
	@PostMapping("/status")
	public void updateUserStatus(@RequestParam String psid, @RequestParam boolean userStatus);
	
	
}
