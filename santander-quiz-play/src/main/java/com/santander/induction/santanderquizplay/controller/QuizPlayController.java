package com.santander.induction.santanderquizplay.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.santander.induction.santanderquizplay.model.QuizSubmission;
import com.santander.induction.santanderquizplay.service.QuizPlayService;

@RestController
public class QuizPlayController {
	
	@Autowired
	private QuizPlayService quizPlayService;
	
	@GetMapping("/quizsubmissions/{submissionId}")
	public QuizSubmission getQuizSubmissionById(@PathVariable("submissionId") UUID submissionId) {
		return quizPlayService.getQuizSubmissionById(submissionId);
	}
	
	@GetMapping("/quizsubmissions")
	public List<QuizSubmission> getAllQuizSubmissions() {
		return quizPlayService.getAllQuizSubmissions();
	}
	//create a new quiz play submission...
	@PostMapping("/quizsubmissions/{psid}/{quizCode}")
	public ResponseEntity<Map<String, Object>> postQuizSubmission(@PathVariable String psid, @PathVariable String quizCode) {
		UUID quizSubmissionId = quizPlayService.postQuizSubmission(psid, quizCode);
		Map<String, Object> map = new HashMap<>();
		map.put("error", false);
		map.put("message", "quiz started successfully..");
		map.put("result", quizSubmissionId);
		System.out.println("Quiz sub id::::::::::::::::---> "+ quizSubmissionId);
		return new ResponseEntity<Map<String,Object>>(map, HttpStatus.OK);
	}
	
	//String psid, UUID submissionId, String answerOption, UUID questionId, Quiz quizCode, boolean lastQuestion
	@PostMapping("quizsubmissions/{psid}/{quizCode}/{questionId}/{submissionId}/{answerOption}/{lastQuestion}")
	public void evaluate(@PathVariable String psid, @PathVariable String quizCode, @PathVariable UUID questionId, 
			@PathVariable UUID submissionId, @PathVariable String answerOption, @PathVariable boolean lastQuestion) {
		
		System.out.println("inside evaluate method..");
		quizPlayService.evaluate(psid, quizCode, questionId, submissionId, answerOption, lastQuestion);
	}
}
