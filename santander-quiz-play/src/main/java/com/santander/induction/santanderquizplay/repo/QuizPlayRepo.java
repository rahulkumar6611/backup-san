package com.santander.induction.santanderquizplay.repo;

import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.santander.induction.santanderquizplay.model.QuizSubmission;

public interface QuizPlayRepo extends MongoRepository<QuizSubmission, UUID>{

	QuizSubmission findBySubmissionId(UUID submissionId);
}
