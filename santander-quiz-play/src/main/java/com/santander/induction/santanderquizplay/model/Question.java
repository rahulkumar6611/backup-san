package com.santander.induction.santanderquizplay.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class Question {

	private UUID questionId;
	private String questionTitle;
	private String questionContent;	
	private ArrayList<AnswerOptions> answerOptions = new ArrayList<>();
	private String answerKey;
	private boolean randomiseOptions;
	private Date createdOn = new Date();
	private enum Domain {
		LOAN,
		PERSONAL_ACCOUNT,
		MORTGAGE
	}
	private Domain domain;

	
	public Question() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Question(String questionTitle, String questionContent, ArrayList<AnswerOptions> answerOptions,
			String answerKey, boolean randomiseOptions, Date createdOn, Domain domain) {
		super();
		this.questionTitle = questionTitle;
		this.questionContent = questionContent;
		this.answerOptions = answerOptions;
		this.answerKey = answerKey;
		this.randomiseOptions = randomiseOptions;
		this.createdOn = createdOn;
		this.domain = domain;
	}



	public UUID getQuestionId() {
		return questionId;
	}
	public void setQuestionId(UUID questionId) {
		this.questionId = questionId;
	}
	public String getQuestionTitle() {
		return questionTitle;
	}
	public void setQuestionTitle(String questionTitle) {
		this.questionTitle = questionTitle;
	}
	public String getQuestionContent() {
		return questionContent;
	}
	public void setQuestionContent(String questionContent) {
		this.questionContent = questionContent;
	}
	public ArrayList<AnswerOptions> getAnswerOptions() {
		return answerOptions;
	}
	public void setAnswerOptions(ArrayList<AnswerOptions> answerOptions) {
		this.answerOptions = answerOptions;
	}
	public String getAnswerKey() {
		return answerKey;
	}
	public void setAnswerKey(String answerKey) {
		this.answerKey = answerKey;
	}
	public boolean isRandomiseOptions() {
		return randomiseOptions;
	}
	public void setRandomiseOptions(boolean randomiseOptions) {
		this.randomiseOptions = randomiseOptions;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	
	public Domain getDomain() {
		return domain;
	}



	public void setDomain(Domain domain) {
		this.domain = domain;
	}



	@Override
	public String toString() {
		return "Question [questionId=" + questionId + ", questionTitle=" + questionTitle + ", questionContent="
				+ questionContent + ", answerOptions=" + answerOptions + ", answerKey=" + answerKey
				+ ", randomiseOptions=" + randomiseOptions + ", createdOn=" + createdOn + ", domain=" + domain + "]";
	}

	

}
