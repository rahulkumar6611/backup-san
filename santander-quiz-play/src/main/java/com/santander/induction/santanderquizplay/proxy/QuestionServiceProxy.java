package com.santander.induction.santanderquizplay.proxy;

import java.util.UUID;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.santander.induction.santanderquizplay.model.Question;

@FeignClient(name="santander-question-inventory")
public interface QuestionServiceProxy {
	
	@GetMapping("/questions/{questionId}")
	Question findByQuestionId(@PathVariable("questionId") UUID questionId);

}
