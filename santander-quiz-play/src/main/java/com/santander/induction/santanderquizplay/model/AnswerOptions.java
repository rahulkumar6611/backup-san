package com.santander.induction.santanderquizplay.model;

public class AnswerOptions {

	private String optionKey;
	private String optionContent;
	
	public AnswerOptions() {
		super();
	}
	public AnswerOptions(String optionKey, String optionContent) {
		super();
		this.optionKey = optionKey;
		this.optionContent = optionContent;
	}
	public String getOptionKey() {
		return optionKey;
	}
	public void setOptionKey(String optionKey) {
		this.optionKey = optionKey;
	}
	public String getOptionContent() {
		return optionContent;
	}
	public void setOptionContent(String optionContent) {
		this.optionContent = optionContent;
	}
	
	
}
