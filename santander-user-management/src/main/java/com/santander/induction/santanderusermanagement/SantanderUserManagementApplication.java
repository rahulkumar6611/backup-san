package com.santander.induction.santanderusermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SantanderUserManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(SantanderUserManagementApplication.class, args);
	}

}
