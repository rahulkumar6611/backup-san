package com.santander.induction.santanderusermanagement.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.santander.induction.santanderusermanagement.model.User;
import com.santander.induction.santanderusermanagement.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping("/users")
	public List<User> getAllUser(User user) {
		ArrayList<User> user1 = new ArrayList<>();
		user1 = (ArrayList<User>) userService.findAll();
		System.out.println(user1);
		return user1;
	}

	@PostMapping("/users")
	public User addNewUser(@RequestBody User user) {
		User newUser = new User();
		newUser = userService.createUser(user);
		return newUser;
	}

	@PostMapping("/status")
	public void updateUserStatus(@RequestParam String psid, @RequestParam boolean userStatus) {
		System.out.println("In patch");
		userService.updateUserStatus(psid, userStatus);		
	}

	@GetMapping("users/{psid}")
	public User findByUserPsid(@PathVariable String psid) {
		return userService.searchByPsid(psid);
	}

	@GetMapping("/userprofile")
	public User getUserProfile(HttpServletRequest request) {
		User user = userService.loadByUsername(request);
		return user;
	}
}