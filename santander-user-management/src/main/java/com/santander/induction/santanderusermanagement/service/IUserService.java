package com.santander.induction.santanderusermanagement.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.santander.induction.santanderusermanagement.model.User;

public interface IUserService {

	User createUser(User user);

	List<User> findAll();

	User searchByPsid(String psid);

	User loadByUsername(HttpServletRequest request);
//	boolean authenticateUser(String psid, String password, HttpServletResponse response) throws Exception;
}
