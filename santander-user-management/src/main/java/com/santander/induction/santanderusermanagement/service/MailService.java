package com.santander.induction.santanderusermanagement.service;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.santander.induction.santanderusermanagement.model.MailModel;

@Service
public class MailService {
	
	
	@Autowired
    private JavaMailSender javaMailSender;

//	public void sendmail() {
//		   Properties props = new Properties();
//		   props.put("mail.smtp.auth", "true");
//		   props.put("mail.smtp.starttls.enable", "true");
//		   props.put("mail.smtp.host", "smtp.gmail.com");
//		   props.put("mail.smtp.port", "587");
		   
//		   Session session = Session.getInstance(props, new javax.mail.Authenticator() {
//		      protected PasswordAuthentication getPasswordAuthentication() {
//		         return new PasswordAuthentication("tutorialspoint@gmail.com", "<your password>");
//		      }
//		   });
//		   Message msg = new MimeMessage(session);
//		   msg.setFrom(new InternetAddress("tutorialspoint@gmail.com", false));
//
//		   msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("tutorialspoint@gmail.com"));
//		   msg.setSubject("Tutorials point email");
//		   msg.setContent("Tutorials point email", "text/html");
//		   msg.setSentDate(new Date());
//
//		   MimeBodyPart messageBodyPart = new MimeBodyPart();
//		   messageBodyPart.setContent("Tutorials point email", "text/html");
//
//		   Multipart multipart = new MimeMultipart();
//		   multipart.addBodyPart(messageBodyPart);
//		   MimeBodyPart attachPart = new MimeBodyPart();
//
//		   attachPart.attachFile("/var/tmp/image19.png");
//		   multipart.addBodyPart(attachPart);
//		   msg.setContent(multipart);
//		   Transport.send(msg);   
		
		
	public void sendmail( String mailTo, String subject, String setText ) {
		SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(mailTo);
        msg.setSubject(subject);
        msg.setText(setText);
        
        javaMailSender.send(msg);
   
	}
	
	
//	 public void sendEmail(MailModel mail) {
//	        MimeMessage mimeMessage = mailSender.createMimeMessage();
//	 
//	        try {
//	 
//	            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
//	 
//	            mimeMessageHelper.setSubject(mail.getMailSubject());
//	            mimeMessageHelper.setFrom(new InternetAddress(mail.getMailFrom(), "technicalkeeda.com"));
//	            mimeMessageHelper.setTo(mail.getMailTo());
//	            mimeMessageHelper.setText(mail.getMailContent());
//	 
//	            mailSender.send(mimeMessageHelper.getMimeMessage());
//	 
//	        } catch (MessagingException e) {
//	            e.printStackTrace();
//	        } catch (UnsupportedEncodingException e) {
//	            e.printStackTrace();
//	        }
//	    }
	
	
	void sendEmailWithAttachment() throws MessagingException, IOException {

        MimeMessage msg = javaMailSender.createMimeMessage();

        // true = multipart message
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		
        helper.setTo("rahul7980jio@email");

        helper.setSubject("Testing from Spring Boot");

        // default = text/plain
        //helper.setText("Check attachment for image!");

        // true = text/html
        helper.setText("<h1>Check attachment for image!</h1>", true);

		// hard coded a file path
        //FileSystemResource file = new FileSystemResource(new File("path/android.png"));

        helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));

        javaMailSender.send(msg);

    }
}
