package com.santander.induction.santanderusermanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.santander.induction.santanderusermanagement.service.MailService;

@RestController
public class MailController {
	
	@Autowired
	private MailService mailService; 

	@GetMapping("/mail")
	public void sendEmail(@RequestParam String mailTo, @RequestParam String subject, @RequestParam String setText) {
	    mailService.sendmail(mailTo, subject, setText);
	} 
}
