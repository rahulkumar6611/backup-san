package com.santander.induction.santanderusermanagement.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.santander.induction.santanderusermanagement.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, String> {

	 User findByPsid(String psid);
	 User findByEmail(String email);
}
