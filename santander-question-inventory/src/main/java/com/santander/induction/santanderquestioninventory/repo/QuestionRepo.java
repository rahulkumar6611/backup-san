package com.santander.induction.santanderquestioninventory.repo;

import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.santander.induction.santanderquestioninventory.model.Question;

@Repository
public interface QuestionRepo extends MongoRepository<Question, UUID> {

	Question findByQuestionId(UUID questionId);
}
