package com.santander.induction.santanderquestioninventory.service;

import java.util.List;
import java.util.UUID;

import com.santander.induction.santanderquestioninventory.model.Question;

public interface IQuestionService {

	List<Question> getAllQuestions();
	Question createQuestion(Question question);
	Question findByQuestionId(UUID questionId);
}
