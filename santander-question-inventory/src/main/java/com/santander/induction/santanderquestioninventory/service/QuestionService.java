package com.santander.induction.santanderquestioninventory.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.santander.induction.santanderquestioninventory.model.Question;
import com.santander.induction.santanderquestioninventory.repo.QuestionRepo;

@Service
public class QuestionService implements IQuestionService {
	
	@Autowired
	private QuestionRepo quesRepo;
	

	@Override
	public List<Question> getAllQuestions() {
		return quesRepo.findAll();
	}

	@Override
	public Question createQuestion(Question question) {
		UUID questionId = UUID.randomUUID();
		question.setQuestionId(questionId);
		return quesRepo.save(question);
	}

	@Override
	public Question findByQuestionId(UUID questionId) {
		return quesRepo.findByQuestionId(questionId);
	}

}
