package com.santander.induction.santanderquestioninventory.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.santander.induction.santanderquestioninventory.model.Question;
import com.santander.induction.santanderquestioninventory.service.QuestionService;

@RestController
public class QuestionController {
	
	@Autowired
	private QuestionService quesService;
	
	
	@GetMapping("/questions")
	public List<Question> getAllQuestions(){
		return quesService.getAllQuestions();
	}
	
	@PostMapping("/questions")
	private void createQuiz(@RequestBody Question question) {
		quesService.createQuestion(question);
	}
	
	@GetMapping("/questions/{questionId}")
	public Question findByQuestionId(@PathVariable("questionId") UUID questionId) {
		return quesService.findByQuestionId(questionId);
	}

}
