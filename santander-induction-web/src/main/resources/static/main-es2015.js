(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header>\r\n\r\n        <div class=\"hImage\">\r\n                <div class=\"headerI\">\r\n                    <a [routerLink]=\"['/dashboard']\"> <img src=\"assets/santander-logo.jpg\"> </a>    \r\n                    \r\n                    </div>\r\n        </div>\r\n    </header>\r\n<router-outlet></router-outlet>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n    <div><app-nav-bar></app-nav-bar></div> -->\r\n    <div fxLayout=\"column\" fxLayoutAlign=\"start center\" fxLayoutGap= \"5px\">\r\n        <!-- <div class=\"san-logo\"> -->\r\n            <!-- <app-san-carousel></app-san-carousel> -->\r\n        <!-- </div> -->\r\n        <div fxLayout=\"row wrap\" fxLayoutAlign=\"space-around\" fxLayoutGap= \"5px\">\r\n            <div *ngFor = \"let data of listOfAllQuizzes\">\r\n                <div fxLayout= \"row wrap\" fxLayoutAlign= \"space-between\" padding=\"4px\">\r\n                    <app-card [quiz]=\"data\" [componentName]= \"dashboard\"></app-card>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>    \r\n<!-- </div>  -->\r\n\r\n<section></section>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/landing-page/landing-page.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/landing-page/landing-page.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<div style=\"background-color:rgb(80, 75, 75)\" class=\"background\" fxLayout=\"row\" fxLayoutAlign=\"space-around center\">\r\n <div>\r\n   <h1>Santander-Quiz</h1>\r\n   <p>- Its a cloud based platform for accessing knowledge and self learning.</p>\r\n   <div fxLayout=\"row\" fxLayoutAlign=\"space-between\" fxLayoutGap=\"20px\" style=\"margin-top:70px;margin-right:750px\">\r\n   <button mat-raised-button style=\"width:120px\" (click)=\"openRegisterDialog()\">Register</button>\r\n   <button mat-raised-button style=\"width:120px\" (click)=\"openLoginDialog()\">Login</button>\r\n  </div>\r\n </div>\r\n</div>\r\n\r\n\r\n <div style=\"margin-top:5%\"  fxLayout=\"row\" fxLayoutAlign=\"space-around center\">\r\n    <div class=\"card\">\r\n        <mat-card>\r\n        <mat-card-content>\r\n         <h3>TAKE</h3>\r\n         <i class=\"material-icons\">\r\n            local_library\r\n            </i>\r\n        </mat-card-content>\r\n      </mat-card>\r\n      </div>\r\n      <div class=\"card\">\r\n      <mat-card>\r\n        <mat-card-content>\r\n          <h3>ANALYZE</h3>\r\n          <i class=\"material-icons\">\r\n              find_in_page\r\n              </i>\r\n        </mat-card-content>\r\n      </mat-card>\r\n      </div>\r\n      <div class=\"card\">\r\n      <mat-card>\r\n        <mat-card-content>\r\n          <h3>VIEW</h3>\r\n          <i class=\"material-icons\">\r\n              visibility\r\n              </i>\r\n        </mat-card-content>\r\n      </mat-card>\r\n      </div>\r\n</div> \r\n\r\n<footer>\r\n  <div style=\"margin-top:50px; margin-left:500px\">\r\n      Santander powered by ITC Infotech\r\n  </div></footer>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <div class=\"example-container\">\r\n <mat-card>\r\n        <mat-card-title>LOGIN</mat-card-title>\r\n  <mat-card-content>\r\n   \r\n      <p>\r\n          <mat-form-field hintLabel=\"Max 5 characters\">\r\n              <input matInput #input maxlength=\"5\" placeholder=\"Enter your PSID\" [(ngModel)]='userPsid' >\r\n              <mat-hint align=\"end\">{{input.value?.length || 0}}/5</mat-hint>\r\n            </mat-form-field>\r\n      </p>\r\n\r\n      <p>\r\n          <mat-form-field>\r\n              <input matInput placeholder=\"Enter your password\" [type]=\"hide ? 'password' : 'text'\" [(ngModel)]='password'>\r\n              <button mat-icon-button matSuffix (click)=\"hide = !hide\" [attr.aria-label]=\"'Hide password'\" [attr.aria-pressed]=\"hide\">\r\n              <mat-icon>{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n              </button>\r\n            </mat-form-field>\r\n      </p>\r\n\r\n    \r\n\r\n      <div class=\"button\">\r\n        <button  mat-button (click)=\"onLogin()\">Login</button>\r\n      </div>\r\n\r\n   \r\n  </mat-card-content>\r\n</mat-card>\r\n</div> -->\r\n<form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\r\n  \r\n    <label>\r\n      PSID:\r\n      <input type=\"text\" formControlName=\"psid\">\r\n    </label>\r\n  \r\n    <label>\r\n      Password:\r\n      <input type=\"password\" formControlName=\"password\">\r\n    </label>\r\n  \r\n    <button type=\"submit\" [disabled]=\"!loginForm.valid\">Submit</button>\r\n  </form>\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/nav-bar/nav-bar.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nav-bar/nav-bar.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<div class=\"side-nav\">\r\n\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/quiz-lobby/quiz-lobby.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/quiz-lobby/quiz-lobby.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <p>quiz-lobby works!</p>\r\n\r\n<mat-card class=\"example-card1\">\r\n    <mat-card-header>\r\n      <div mat-card-avatar class=\"example-header-image1\"></div>\r\n      <mat-card-title>{{quiz.quizCode}}</mat-card-title>\r\n      <mat-card-subtitle>{{quiz.quizName}}</mat-card-subtitle>\r\n    </mat-card-header>\r\n    <mat-card-content>\r\n      <p>\r\n        {{quiz.description}}\r\n      </p>\r\n    </mat-card-content>\r\n    <mat-card-actions>\r\n      <button mat-button (click)= \"onCancel()\">Cancel</button>\r\n      <button mat-button (click)=\"startQuiz()\" >Play Quiz</button>\r\n      <mat-icon>query_builder</mat-icon>\r\n     <button mat-raised-button color=\"red\">Primary</button> \r\n    </mat-card-actions>\r\n  </mat-card> -->\r\n\r\n<div class=\"cards-set\">\r\n  <div class=\"card\">\r\n    <div class=\"heading\" style=\"background-color: #8f8c8d\">\r\n      <mat-card fxLayout=\"row\" fxLayoutAlign=\"space-evenly center\" >\r\n        <div>\r\n          <mat-card-header>\r\n              <div mat-card-avatar class=\"example-header-image1\"></div>\r\n              <mat-card-title>{{quiz.quizCode}}</mat-card-title>\r\n              <mat-card-subtitle>{{quiz.quizName}}</mat-card-subtitle>\r\n            </mat-card-header>\r\n            <mat-card-content>\r\n                <p><span><mat-icon>timer</mat-icon>{{quiz.maxDurationMinutes}}</span>\r\n                  <span><mat-icon> flag </mat-icon>{{quiz.maxAttempts}}</span>\r\n                  <span><mat-icon> star </mat-icon>{{quiz.maxScore}}</span>\r\n                </p>\r\n              <p>{{quiz.description}}</p>\r\n            </mat-card-content>\r\n          </div>\r\n          <div>\r\n            <mat-card-actions fxLayout=\"column\" fxLayoutAlign=\"space-around center\" fxLayoutGap=\"10px\">\r\n              <button mat-button (click)=\"startQuiz()\" >Play Quiz</button>\r\n              <button mat-button (click)= \"onCancel()\">Cancel</button>\r\n            </mat-card-actions>\r\n          </div>\r\n        </mat-card>\r\n      </div>\r\n      \r\n      <mat-tab-group >\r\n          <mat-tab label=\"TOPICS\"> \r\n              <div *ngFor= \"let concept of quiz.concepts\">\r\n                {{concept}}\r\n              </div>\r\n          </mat-tab>\r\n          <mat-tab label=\"PRE-REQUISITES\">\r\n            <ul>\r\n              <li>Basic knowledge of java</li>\r\n               <li>HTML and CSS should be proficient</li>\r\n               <li>Angular Material should be done</li>\r\n            </ul>\r\n          </mat-tab>\r\n          <mat-tab label=\"REWARDS\"> \r\n            <ul>\r\n              <li>Currently, No rewards is aplicable.</li>\r\n            </ul>\r\n             </mat-tab>\r\n        </mat-tab-group>\r\n        \r\n  </div>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/registration/registration.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/registration/registration.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n\r\n\r\n<div class=\"example-container\">\r\n        <mat-card>\r\n                <mat-card-title>REGISTER HERE</mat-card-title>\r\n          <mat-card-content>\r\n          \r\n              <p>\r\n        <mat-form-field>\r\n          <input matInput placeholder=\"Enter your email\" [formControl]=\"email\" required>\r\n          <mat-error *ngIf=\"email.invalid\">{{getErrorMessage()}}</mat-error>\r\n        </mat-form-field>\r\n     </p>\r\n     <p>\r\n        <mat-form-field hintLabel=\"Max 5 characters\">\r\n                <input matInput #input maxlength=\"5\" placeholder=\"Enter your PSID\">\r\n                <mat-hint align=\"end\">{{input.value?.length || 0}}/5</mat-hint>\r\n    </mat-form-field>\r\n</p>\r\n<p>\r\n              <mat-form-field>\r\n                    <input matInput placeholder=\"Enter your password\" [type]=\"hide ? 'password' : 'text'\">\r\n                    <button mat-icon-button matSuffix (click)=\"hide = !hide\" [attr.aria-label]=\"'Hide password'\" [attr.aria-pressed]=\"hide\">\r\n                    <mat-icon>{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                    </button>\r\n                  </mat-form-field>\r\n                </p>\r\n\r\n                <div class=\"button\">\r\n                        <button  mat-button (click)=\"submit()\">Register</button>\r\n                       \r\n                      </div>\r\n                      {{msg}}\r\n      \r\n    </mat-card-content>\r\n</mat-card>\r\n\r\n      </div>\r\n      ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/result/result.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/result/result.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>result works!</p>\r\n\r\n\r\n<p>Total Marks: {{totalMarks}}</p>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/san-carousel/san-carousel.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/san-carousel/san-carousel.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ngb-carousel *ngIf=\"images\">\r\n    <ng-template ngbSlide>\r\n      <div class=\"picsum-img-wrapper\">\r\n        <img [src]=\"images[0]\" alt=\"Random first slide\">\r\n      </div>\r\n      <div class=\"carousel-caption\">\r\n        <h3>First slide label</h3>\r\n        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>\r\n      </div>\r\n    </ng-template>\r\n    <ng-template ngbSlide>\r\n      <div class=\"picsum-img-wrapper\">\r\n        <img [src]=\"images[1]\" alt=\"Random second slide\">\r\n      </div>\r\n      <div class=\"carousel-caption\">\r\n        <h3>Second slide label</h3>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n      </div>\r\n    </ng-template>\r\n<ng-template ngbSlide> -->\r\n        <ngb-carousel *ngIf=\"images\">\r\n                <ng-template ngbSlide>\r\n                  <div class=\"picsum-img-wrapper\">\r\n                    <!-- <img [src]=\"images[0]\" alt=\"Random first slide\"> -->\r\n                  </div>\r\n                  <div class=\"carousel-caption\">\r\n                    <h3>Santander Induction</h3>\r\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis laboriosam dolore repellendus tempora. Cupiditate eligendi ut ad doloribus nam dolorem.</p>\r\n                  </div>\r\n                </ng-template>\r\n                <ng-template ngbSlide>\r\n                  <div class=\"picsum-img-wrapper\">\r\n                    <!-- <img [src]=\"images[1]\" alt=\"Random second slide\"> -->\r\n                  </div>\r\n                  <div class=\"carousel-caption\">\r\n                    <h3>ITC Infotech</h3>\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n                  </div>\r\n                </ng-template>\r\n                <ng-template ngbSlide>\r\n                  <div class=\"picsum-img-wrapper\">\r\n                    <!-- <img [src]=\"images[2]\" alt=\"Random third slide\"> -->\r\n                  </div>\r\n                  <div class=\"carousel-caption\">\r\n                    <h3>Third slide label</h3>\r\n                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>\r\n                  </div>\r\n                </ng-template>\r\n              </ngb-carousel>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared-component/card/card.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared-component/card/card.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"quiz!=null\">\r\n  <mat-card class= \"quiz-Card-1\" (click)=\"onSelect()\" >\r\n    <mat-card-actions>\r\n      <mat-card-header>\r\n        <mat-card-title>{{quiz.quizCode | uppercase}}</mat-card-title>\r\n        <mat-card-subtitle> {{quiz.quizName | uppercase}}</mat-card-subtitle>\r\n      </mat-card-header>\r\n      \r\n      <!-- <mat-card-footer class=\"card-footer1\">\r\n      </mat-card-footer>   -->\r\n    </mat-card-actions>\r\n  </mat-card>\r\n</div>\r\n\r\n\r\n\r\n\r\n<!-- <div *ngIf=\"quiz!=null\">\r\n    <mat-card class=\"quiz-card\">\r\n      <mat-card-header fxLayout=\"row\" fxLayoutAlign=\"space-between\" class=\"header-color\">\r\n  \r\n        <mat-card-title matTooltip=\"Quiz Title\">{{ quiz.title | uppercase }} </mat-card-title>\r\n        <mat-card-subtitle matTooltip=\"Quiz code\">{{quiz.quizCode}} create on {{quiz.createDate}}\r\n        </mat-card-subtitle> -->\r\n        <!-- <mat-card-subtitle> By {{quiz.name | titlecase}} {{quiz.createDate}}</mat-card-subtitle> -->\r\n        <!-- <div style=\"margin-right: 25px;\">\r\n          <button mat-icon-button (click)=\"addToFavourite(quiz.quizCode)\"> -->\r\n            <!-- <mat-icon class=\"fav-icon\" *ngIf=\"fav==false\" matTooltip=\"Click to bookmark\">favorite_border</mat-icon> -->\r\n            <!-- <mat-icon class=\"fav-icon\" *ngIf=\"fav==true\" matTooltip=\"Click to bookmark\" style=\"color: red\">favorite\r\n            </mat-icon>\r\n          </button>\r\n        </div>\r\n      </mat-card-header>\r\n      <mat-divider></mat-divider>\r\n      <mat-card-content class=\"card-content\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"space-between start\" class=\"subject-icon\">\r\n          <div style=\"word-break: break-all\" class=\"description-view\"> {{quiz.description}} </div>\r\n          <div style=\"margin-right: 20px\">\r\n            <button mat-icon-button matTooltip=\"Click to play\" color=\"primary\" (click)=\"onSelect()\">\r\n              <mat-icon style=\"font-size: 60px; margin-bottom: 10px\">play_circle_filled</mat-icon>\r\n            </button>\r\n          </div>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\" class=\"card-icons\">\r\n          <div class=\"icon-flex\">\r\n            <mat-icon class=\"nav-icon\" matTooltip=\"Timer\">query_builder</mat-icon>\r\n            <div class=\"icon-space\">{{quiz.maxDurationMinutes}} min </div>\r\n          </div>\r\n          <div class=\"icon-flex\">\r\n            <mat-icon class=\"nav-icon\" matTooltip=\"{{quiz.maxScore}} points\">emoji_events</mat-icon>\r\n            <div class=\"icon-space\"> {{quiz.maxScore}} points</div>\r\n          </div>\r\n        </div> -->\r\n        <!-- for dispalaying concepts.. -->\r\n        <!-- <mat-chip-list>\r\n          <div *ngIf=\"quiz.concepts!=null\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n            <mat-chip *ngFor=\"let concept of quiz.concepts\" class=\"subject-chip\">{{concept | titlecase}}\r\n            </mat-chip>\r\n          </div>\r\n        </mat-chip-list>\r\n      </mat-card-content>\r\n    </mat-card>\r\n  </div> -->\r\n  \r\n\r\n  <!-- <mat-card class=\"example-card1\">\r\n    <mat-card-header>\r\n      <div mat-card-avatar class=\"example-header-image1\"></div>\r\n      <mat-card-title>Shiba Inu</mat-card-title>\r\n      <mat-card-subtitle>Dog Breed</mat-card-subtitle>\r\n    </mat-card-header>\r\n    <img mat-card-image src=\"https://material.angular.io/assets/img/examples/shiba2.jpg\" alt=\"Photo of a Shiba Inu\">\r\n    <mat-card-content>\r\n      <p>\r\n        The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan.\r\n        A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally\r\n        bred for hunting.\r\n      </p>\r\n    </mat-card-content>\r\n    <mat-card-actions>\r\n      <button mat-button>LIKE</button>\r\n      <button mat-button>SHARE</button>\r\n      <mat-icon>query_builder</mat-icon> -->\r\n      <!-- <button mat-raised-button color=\"red\">Primary</button> -->\r\n    <!-- </mat-card-actions>\r\n  </mat-card> -->");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared-component/header/header.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared-component/header/header.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>header works!</p>\r\n<header>\r\n\r\n</header>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/test-window/test-window.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/test-window/test-window.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<!-- <div *ngFor = \"let ques of question\">\r\n <mat-card class=\"example-card1\">\r\n    <mat-card-header>\r\n      <mat-card-title>{{ques.questionId}}</mat-card-title>\r\n      <mat-card-subtitle>{{ques.questionTitle}}</mat-card-subtitle>\r\n    </mat-card-header>\r\n    <mat-card-content>\r\n      <p>\r\n        {{ques.questionContent}}\r\n      </p>\r\n      <div *ngFor= \"let answerOption of ques.answerOptions\">\r\n            <p>{{answerOption.optionContent}}</p>\r\n      </div>\r\n    </mat-card-content>\r\n    <button mat-icon-button (click)= \"onNextButtonClick()\"></button>\r\n  </mat-card>\r\n</div> -->\r\n\r\n\r\n<div fxLayout=\"row\" fxLayoutAlign=\"space-between center\" style=\"padding-left:20px; padding-right:30px\">\r\n  <div>\r\n  <h1>{{quizName}}</h1></div>\r\n  <div>\r\n  <p><span><mat-icon>timer</mat-icon>{{quizDuration}} sec left.</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"cards-set\">\r\n    <div class=\"card\">\r\n      <div class=\"heading\" style=\"background-color: #ffffff\">\r\n        <mat-card fxLayout=\"row\" fxLayoutAlign=\"center center\"  >\r\n          <div>\r\n            <!-- <mat-card-header>\r\n                <div mat-card-avatar class=\"example-header-image1\"></div>\r\n              \r\n              </mat-card-header> -->\r\n              <mat-card-content>\r\n                  <label id=\"example-radio-group-label\"> \r\n                      Q. {{question.questionTitle}}\r\n                  </label>\r\n                  <mat-radio-group\r\n                    aria-labelledby=\"example-radio-group-label\"\r\n                    class=\"example-radio-group\"\r\n                    [(ngModel)]=\"userSelectedOption\" >\r\n                    <mat-radio-button class=\"example-radio-button\" \r\n                            *ngFor= \"let answerOption of question.answerOptions\" \r\n                            [value]=\"answerOption.optionContent\">\r\n                        {{answerOption.optionContent}}\r\n                        \r\n                    </mat-radio-button>\r\n                  </mat-radio-group>\r\n                  {{userSelectedOption}}\r\n              </mat-card-content>\r\n              <div>\r\n                  <mat-card-actions  fxLayout=\"row\" fxLayoutAlign=\"end end\" fxLayoutGap=\"20px\">\r\n                    <button mat-button (click)=\"onNextButtonClick()\" *ngIf= \"!lastQuestion\">Next</button>\r\n                    <button mat-button (click)=\"onNextButtonClick()\" *ngIf= \"lastQuestion\">Finish</button>\r\n                    <!-- <button mat-button (click)= \"onCancel()\">Skip</button> -->\r\n                  </mat-card-actions>\r\n                </div>\r\n            </div>\r\n            \r\n          </mat-card>\r\n\r\n          <div style=\"margin-top:25px\">\r\n              <mat-card-actions  fxLayout=\"row\" fxLayoutAlign=\"center center\" >\r\n                  <button mat-button (click)=\"endQuiz()\" style=\"color:red\">End Test</button>    \r\n              </mat-card-actions>\r\n          </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule, routingComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routingComponents", function() { return routingComponents; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var src_app_quiz_lobby_quiz_lobby_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/quiz-lobby/quiz-lobby.component */ "./src/app/quiz-lobby/quiz-lobby.component.ts");
/* harmony import */ var src_app_test_window_test_window_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/test-window/test-window.component */ "./src/app/test-window/test-window.component.ts");
/* harmony import */ var src_app_result_result_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/result/result.component */ "./src/app/result/result.component.ts");
/* harmony import */ var src_app_login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var src_app_landing_page_landing_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/landing-page/landing-page.component */ "./src/app/landing-page/landing-page.component.ts");
/* harmony import */ var src_app_registration_registration_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/registration/registration.component */ "./src/app/registration/registration.component.ts");










const routes = [
    { path: '', component: src_app_landing_page_landing_page_component__WEBPACK_IMPORTED_MODULE_8__["LandingPageComponent"] },
    { path: 'login', component: src_app_login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"] },
    { path: 'register', component: src_app_registration_registration_component__WEBPACK_IMPORTED_MODULE_9__["RegistrationComponent"] },
    { path: 'quizlobby/:id', component: src_app_quiz_lobby_quiz_lobby_component__WEBPACK_IMPORTED_MODULE_4__["QuizLobbyComponent"] },
    { path: 'testwindow/:quizCode', component: src_app_test_window_test_window_component__WEBPACK_IMPORTED_MODULE_5__["TestWindowComponent"] },
    { path: 'dashboard', component: src_app_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"] },
    { path: 'result/:id', component: src_app_result_result_component__WEBPACK_IMPORTED_MODULE_6__["ResultComponent"] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { useHash: true })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);

const routingComponents = [
    src_app_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"],
    src_app_quiz_lobby_quiz_lobby_component__WEBPACK_IMPORTED_MODULE_4__["QuizLobbyComponent"],
    src_app_test_window_test_window_component__WEBPACK_IMPORTED_MODULE_5__["TestWindowComponent"],
    src_app_result_result_component__WEBPACK_IMPORTED_MODULE_6__["ResultComponent"],
    src_app_login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"],
    src_app_landing_page_landing_page_component__WEBPACK_IMPORTED_MODULE_8__["LandingPageComponent"],
    src_app_registration_registration_component__WEBPACK_IMPORTED_MODULE_9__["RegistrationComponent"]
];


/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\nimg{\r\n    height: 55px;\r\n    width: 12%;\r\n    \r\n}\r\n\r\n.hImage{\r\n    background: black;\r\n    \r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0lBQ0ksWUFBWTtJQUNaLFVBQVU7O0FBRWQ7O0FBRUE7SUFDSSxpQkFBaUI7O0FBRXJCIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuaW1ne1xyXG4gICAgaGVpZ2h0OiA1NXB4O1xyXG4gICAgd2lkdGg6IDEyJTtcclxuICAgIFxyXG59XHJcblxyXG4uaEltYWdle1xyXG4gICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgICBcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'SantanderWebApp';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _shared_component_shared_component_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared-component/shared-component.module */ "./src/app/shared-component/shared-component.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./material-module */ "./src/app/material-module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _test_window_test_window_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./test-window/test-window.component */ "./src/app/test-window/test-window.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _result_result_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./result/result.component */ "./src/app/result/result.component.ts");
/* harmony import */ var _nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./nav-bar/nav-bar.component */ "./src/app/nav-bar/nav-bar.component.ts");
/* harmony import */ var _san_carousel_san_carousel_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./san-carousel/san-carousel.component */ "./src/app/san-carousel/san-carousel.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var _landing_page_landing_page_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./landing-page/landing-page.component */ "./src/app/landing-page/landing-page.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/registration/registration.component.ts");
/* harmony import */ var _auth_interceptor_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./auth-interceptor.service */ "./src/app/auth-interceptor.service.ts");










// import { QuizLobbyComponent } from './quiz-lobby/quiz-lobby.component';
// import { QuizPlayComponent } from './quiz-play/quiz-play.component';
// import {
//   MatButtonModule,
//   MatCardModule,
//   MatTabsModule
// } from "@angular/material";










let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["routingComponents"],
            _test_window_test_window_component__WEBPACK_IMPORTED_MODULE_9__["TestWindowComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_10__["LoginComponent"],
            _result_result_component__WEBPACK_IMPORTED_MODULE_11__["ResultComponent"],
            _nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_12__["NavBarComponent"],
            _san_carousel_san_carousel_component__WEBPACK_IMPORTED_MODULE_13__["SanCarouselComponent"],
            _landing_page_landing_page_component__WEBPACK_IMPORTED_MODULE_15__["LandingPageComponent"],
            _registration_registration_component__WEBPACK_IMPORTED_MODULE_17__["RegistrationComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _shared_component_shared_component_module__WEBPACK_IMPORTED_MODULE_5__["SharedComponentModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
            _material_module__WEBPACK_IMPORTED_MODULE_7__["DemoMaterialModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_14__["NgbCarouselModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__["BrowserAnimationsModule"]
        ],
        exports: [_material_module__WEBPACK_IMPORTED_MODULE_7__["DemoMaterialModule"]],
        providers: [
            {
                provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"], useClass: _auth_interceptor_service__WEBPACK_IMPORTED_MODULE_18__["AuthInterceptorService"], multi: true
            }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/auth-interceptor.service.ts":
/*!*********************************************!*\
  !*** ./src/app/auth-interceptor.service.ts ***!
  \*********************************************/
/*! exports provided: AuthInterceptorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptorService", function() { return AuthInterceptorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");






let AuthInterceptorService = class AuthInterceptorService {
    constructor(router) {
        this.router = router;
    }
    intercept(request, next) {
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])((err) => {
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpErrorResponse"]) {
                if (err.status === 403 || err.status === 401) {
                    this.router.navigate(['/']);
                }
            }
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(err);
        }));
    }
};
AuthInterceptorService.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AuthInterceptorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthInterceptorService);



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".san-logo{\r\n    width: 100%;\r\n    height: 55vh;\r\n    background:linear-gradient(135deg, rgb(119, 116, 116) 0%,lightgray 100%);\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksV0FBVztJQUNYLFlBQVk7SUFDWix3RUFBd0U7QUFDNUUiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2FuLWxvZ297XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNTV2aDtcclxuICAgIGJhY2tncm91bmQ6bGluZWFyLWdyYWRpZW50KDEzNWRlZywgcmdiKDExOSwgMTE2LCAxMTYpIDAlLGxpZ2h0Z3JheSAxMDAlKTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_dashboard_quiz_inventory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/dashboard/quiz-inventory.service */ "./src/app/dashboard/quiz-inventory.service.ts");



let DashboardComponent = class DashboardComponent {
    constructor(quizInventoryService) {
        this.quizInventoryService = quizInventoryService;
        this.dashboard = 'dashboard';
    }
    ngOnInit() {
        this.getQuizzes();
    }
    getQuizzes() {
        this.quizInventoryService.getQuizzes().subscribe(data => {
            this.listOfAllQuizzes = data;
            console.log("quizzes in the system::-->", this.listOfAllQuizzes);
        });
    }
};
DashboardComponent.ctorParameters = () => [
    { type: src_app_dashboard_quiz_inventory_service__WEBPACK_IMPORTED_MODULE_2__["QuizInventoryService"] }
];
DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/dashboard.component.css")).default]
    })
], DashboardComponent);



/***/ }),

/***/ "./src/app/dashboard/quiz-inventory.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/dashboard/quiz-inventory.service.ts ***!
  \*****************************************************/
/*! exports provided: QuizInventoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuizInventoryService", function() { return QuizInventoryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let QuizInventoryService = class QuizInventoryService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        // Url = 'http://localhost:8060/quizzes';
        this.Url1 = 'quiz/quizzes';
    }
    getQuizzes() {
        return this.httpClient.get(this.Url1);
    }
};
QuizInventoryService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
QuizInventoryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], QuizInventoryService);



/***/ }),

/***/ "./src/app/landing-page/landing-page.component.css":
/*!*********************************************************!*\
  !*** ./src/app/landing-page/landing-page.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".background{\r\n  width:100%;\r\n  height: 450px;\r\n}\r\n\r\n.card{\r\n  background-color: white;\r\n  width: 20%;\r\n  height: 150px;\r\n}\r\n\r\nh1{\r\n  font-size: 60px;\r\n  color:  white;\r\n  -webkit-box-align: center;\r\n          align-items: center;\r\n  -webkit-box-pack: center;\r\n          justify-content: center;\r\n}\r\n\r\nh3{\r\n  color: rgb(80, 75, 75);\r\n}\r\n\r\np{\r\n  color: white;\r\n  font-size: 30px;\r\n}\r\n\r\nfooter{\r\n  color: white;\r\n  font-size: 20px;\r\n  background-color: black;\r\n  height: 30px;\r\n  \r\n}\r\n\r\n.material-icons{\r\n  font-size: 67px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGFuZGluZy1wYWdlL2xhbmRpbmctcGFnZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsVUFBVTtFQUNWLGFBQWE7QUFDZjs7QUFFQTtFQUNFLHVCQUF1QjtFQUN2QixVQUFVO0VBQ1YsYUFBYTtBQUNmOztBQUdBO0VBQ0UsZUFBZTtFQUNmLGFBQWE7RUFDYix5QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLHdCQUF1QjtVQUF2Qix1QkFBdUI7QUFDekI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxZQUFZO0VBQ1osZUFBZTtBQUNqQjs7QUFFQTtFQUNFLFlBQVk7RUFDWixlQUFlO0VBQ2YsdUJBQXVCO0VBQ3ZCLFlBQVk7O0FBRWQ7O0FBQ0E7RUFDRSxlQUFlO0FBQ2pCIiwiZmlsZSI6InNyYy9hcHAvbGFuZGluZy1wYWdlL2xhbmRpbmctcGFnZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJhY2tncm91bmR7XHJcbiAgd2lkdGg6MTAwJTtcclxuICBoZWlnaHQ6IDQ1MHB4O1xyXG59XHJcblxyXG4uY2FyZHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICB3aWR0aDogMjAlO1xyXG4gIGhlaWdodDogMTUwcHg7XHJcbn1cclxuXHJcblxyXG5oMXtcclxuICBmb250LXNpemU6IDYwcHg7XHJcbiAgY29sb3I6ICB3aGl0ZTtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcbmgze1xyXG4gIGNvbG9yOiByZ2IoODAsIDc1LCA3NSk7XHJcbn1cclxuXHJcbnB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZvbnQtc2l6ZTogMzBweDtcclxufVxyXG5cclxuZm9vdGVye1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIFxyXG59XHJcbi5tYXRlcmlhbC1pY29uc3tcclxuICBmb250LXNpemU6IDY3cHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/landing-page/landing-page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/landing-page/landing-page.component.ts ***!
  \********************************************************/
/*! exports provided: LandingPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingPageComponent", function() { return LandingPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var src_app_registration_registration_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/registration/registration.component */ "./src/app/registration/registration.component.ts");





let LandingPageComponent = class LandingPageComponent {
    constructor(dialog) {
        this.dialog = dialog;
    }
    ngOnInit() {
    }
    openLoginDialog() {
        this.dialog.open(src_app_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"], {
            width: '600px',
            height: '500px'
        });
    }
    openRegisterDialog() {
        this.dialog.open(src_app_registration_registration_component__WEBPACK_IMPORTED_MODULE_4__["RegistrationComponent"], {
            width: '600px',
            height: '500px'
        });
    }
};
LandingPageComponent.ctorParameters = () => [
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] }
];
LandingPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-landing-page',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./landing-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/landing-page/landing-page.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./landing-page.component.css */ "./src/app/landing-page/landing-page.component.css")).default]
    })
], LandingPageComponent);



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\r\n    display: -webkit-box;\r\n    display: flex;\r\n    -webkit-box-pack: center;\r\n            justify-content: center;\r\n    margin: 100px 0px;\r\n  }\r\n\r\n  .button {\r\n   \r\n    display: -webkit-box;\r\n   \r\n    display: flex;\r\n    -webkit-box-pack: end;\r\n            justify-content: flex-end;\r\n \r\n  }\r\n\r\n  .example-container {\r\n    display: -webkit-box;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n            flex-direction: column;\r\n    width: 100%;\r\n    height: 250px;\r\n  }\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  .example-right-align {\r\n    text-align: right;\r\n  }\r\n\r\n  input.example-right-align::-webkit-outer-spin-button,\r\n  input.example-right-align::-webkit-inner-spin-button {\r\n    display: none;\r\n  }\r\n\r\n  input.example-right-align {\r\n    -moz-appearance: textfield;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLG9CQUFhO0lBQWIsYUFBYTtJQUNiLHdCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIsaUJBQWlCO0VBQ25COztFQUVBOztJQUVFLG9CQUFhOztJQUFiLGFBQWE7SUFDYixxQkFBeUI7WUFBekIseUJBQXlCOztFQUUzQjs7RUFFQTtJQUNFLG9CQUFhO0lBQWIsYUFBYTtJQUNiLDRCQUFzQjtJQUF0Qiw2QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLFdBQVc7SUFDWCxhQUFhO0VBQ2Y7O0VBRUE7SUFDRSxXQUFXO0VBQ2I7O0VBRUE7SUFDRSxpQkFBaUI7RUFDbkI7O0VBRUE7O0lBRUUsYUFBYTtFQUNmOztFQUVBO0lBQ0UsMEJBQTBCO0VBQzVCIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMTAwcHggMHB4O1xyXG4gIH1cclxuXHJcbiAgLmJ1dHRvbiB7XHJcbiAgIFxyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiBcclxuICB9XHJcblxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAyNTBweDtcclxuICB9XHJcbiAgXHJcbiAgLmV4YW1wbGUtY29udGFpbmVyID4gKiB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgXHJcbiAgLmV4YW1wbGUtcmlnaHQtYWxpZ24ge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgfVxyXG4gIFxyXG4gIGlucHV0LmV4YW1wbGUtcmlnaHQtYWxpZ246Oi13ZWJraXQtb3V0ZXItc3Bpbi1idXR0b24sXHJcbiAgaW5wdXQuZXhhbXBsZS1yaWdodC1hbGlnbjo6LXdlYmtpdC1pbm5lci1zcGluLWJ1dHRvbiB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuICBcclxuICBpbnB1dC5leGFtcGxlLXJpZ2h0LWFsaWduIHtcclxuICAgIC1tb3otYXBwZWFyYW5jZTogdGV4dGZpZWxkO1xyXG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var src_app_login_user_login_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/login/user-login.service */ "./src/app/login/user-login.service.ts");






let LoginComponent = class LoginComponent {
    // hide =true;
    // public data1;
    // public userPsid;
    // public password;
    constructor(router, dialogRef, loginService) {
        this.router = router;
        this.dialogRef = dialogRef;
        this.loginService = loginService;
        this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            psid: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
        });
    }
    ngOnInit() {
        console.log("login-component inside ngonit......");
    }
    onSubmit() {
        // TODO: Use EventEmitter with form value
        console.log(this.loginForm.value);
        this.loginService.checkClientData(this.loginForm.value).subscribe((data) => {
            console.log('login-component====>');
            const response = data;
            console.log('statusCODE??????????????????????????' + response.statusCode);
            if (response.statusCode === 200) {
                this.router.navigate(['/dashboard']);
            }
            else {
                this.router.navigate(['']);
            }
            this.dialogRef.close();
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"] },
    { type: src_app_login_user_login_service__WEBPACK_IMPORTED_MODULE_5__["UserLoginService"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")).default]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/login/user-login.service.ts":
/*!*********************************************!*\
  !*** ./src/app/login/user-login.service.ts ***!
  \*********************************************/
/*! exports provided: UserLoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserLoginService", function() { return UserLoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let UserLoginService = class UserLoginService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    validateUser(url) {
        return this.httpClient.get(url);
    }
    checkClientData(user) {
        return this.httpClient.post(`user/login`, user);
    }
};
UserLoginService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
UserLoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UserLoginService);



/***/ }),

/***/ "./src/app/material-module.ts":
/*!************************************!*\
  !*** ./src/app/material-module.ts ***!
  \************************************/
/*! exports provided: DemoMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DemoMaterialModule", function() { return DemoMaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/a11y */ "./node_modules/@angular/cdk/esm2015/a11y.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm2015/drag-drop.js");
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/portal */ "./node_modules/@angular/cdk/esm2015/portal.js");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/esm2015/scrolling.js");
/* harmony import */ var _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/stepper */ "./node_modules/@angular/cdk/esm2015/stepper.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/esm2015/table.js");
/* harmony import */ var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/tree */ "./node_modules/@angular/cdk/esm2015/tree.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm2015/autocomplete.js");
/* harmony import */ var _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/badge */ "./node_modules/@angular/material/esm2015/badge.js");
/* harmony import */ var _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/bottom-sheet */ "./node_modules/@angular/material/esm2015/bottom-sheet.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm2015/button.js");
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/button-toggle */ "./node_modules/@angular/material/esm2015/button-toggle.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm2015/card.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm2015/checkbox.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/esm2015/chips.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm2015/stepper.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm2015/datepicker.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/esm2015/divider.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm2015/expansion.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm2015/grid-list.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm2015/icon.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm2015/input.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm2015/list.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm2015/menu.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm2015/core.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm2015/paginator.js");
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/progress-bar */ "./node_modules/@angular/material/esm2015/progress-bar.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/esm2015/progress-spinner.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/esm2015/radio.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm2015/select.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/esm2015/sidenav.js");
/* harmony import */ var _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @angular/material/slider */ "./node_modules/@angular/material/esm2015/slider.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm2015/slide-toggle.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/esm2015/snack-bar.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm2015/sort.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm2015/table.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm2015/tabs.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm2015/toolbar.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm2015/tooltip.js");
/* harmony import */ var _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! @angular/material/tree */ "./node_modules/@angular/material/esm2015/tree.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm2015/form-field.js");












































const modules = [
    _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_2__["A11yModule"],
    _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
    _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
    _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
    _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
    _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
    _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
    _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
    _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
    _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
    _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
    _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
    _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
    _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
    _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
    _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
    _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
    _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
    _angular_material_form_field__WEBPACK_IMPORTED_MODULE_43__["MatFormFieldModule"],
    _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
    _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
    _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
    _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
    _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
    _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
    _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
    _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
    _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
    _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
    _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
    _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
    _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
    _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
    _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
    _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
    _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
    _angular_material_form_field__WEBPACK_IMPORTED_MODULE_43__["MatFormFieldModule"],
    _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
    _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
    _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
    _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
    _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
    _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
    _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"]
];
let DemoMaterialModule = class DemoMaterialModule {
};
DemoMaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: modules,
        exports: modules
    })
], DemoMaterialModule);

/**  Copyright 2019 Google LLC. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */ 


/***/ }),

/***/ "./src/app/nav-bar/nav-bar.component.css":
/*!***********************************************!*\
  !*** ./src/app/nav-bar/nav-bar.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".side-nav{\r\n    width: 5vw;\r\n    height: 100vh;\r\n    background: black;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmF2LWJhci9uYXYtYmFyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxVQUFVO0lBQ1YsYUFBYTtJQUNiLGlCQUFpQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL25hdi1iYXIvbmF2LWJhci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNpZGUtbmF2e1xyXG4gICAgd2lkdGg6IDV2dztcclxuICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/nav-bar/nav-bar.component.ts":
/*!**********************************************!*\
  !*** ./src/app/nav-bar/nav-bar.component.ts ***!
  \**********************************************/
/*! exports provided: NavBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavBarComponent", function() { return NavBarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NavBarComponent = class NavBarComponent {
    constructor() { }
    ngOnInit() {
    }
};
NavBarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-nav-bar',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./nav-bar.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/nav-bar/nav-bar.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./nav-bar.component.css */ "./src/app/nav-bar/nav-bar.component.css")).default]
    })
], NavBarComponent);



/***/ }),

/***/ "./src/app/quiz-lobby/quiz-lobby.component.css":
/*!*****************************************************!*\
  !*** ./src/app/quiz-lobby/quiz-lobby.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("mat-card-title {\r\n    /* margin: 0;\r\n    padding: 0; */\r\n   \r\n    font:italic 30px Georgia, serif;\r\n    /* font-size: 70%; line-height:80%; */\r\n    margin-bottom: 180px;\r\n    /* margin-left:20px; */\r\n    \r\n  }\r\n   /* p {\r\n    margin: 0.8em 0;\r\n    padding: 0;\r\n  }  */\r\n   .cards-set {\r\n    font-size: 0;\r\n  }\r\n   .card {\r\n    display: inline-block;\r\n    margin: 8px;\r\n    margin-left: 60px;\r\n    width: 90%;\r\n    min-height: 280px;\r\n    font-size: 16px;\r\n    background-color: #ffffff;\r\n    border-radius: 2px;\r\n    box-shadow: 0 12px 15px 0 rgba(0, 0, 0, 0.24);\r\n  }\r\n   .card .heading {\r\n    position: relative;\r\n    height: 250px;\r\n    color:black;\r\n  }\r\n   .card .heading h1 {\r\n    position: absolute;\r\n    bottom: 16px;\r\n    left: 16px;\r\n    font-size: 24px;\r\n    \r\n  }\r\n   .card .content {\r\n    padding: 16px;\r\n  }\r\n   mat-card-actions{\r\n      align-content: right;\r\n  }\r\n   .example-stretched-tabs {\r\n    max-width: 800px;\r\n  }\r\n   mat-tab-group{\r\n      \r\n      -webkit-box-align: flex;\r\n      \r\n              align-items: flex;\r\n      -webkit-box-pack: justify;\r\n              justify-content: space-between;\r\n  }\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcXVpei1sb2JieS9xdWl6LWxvYmJ5LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSTtpQkFDYTs7SUFFYiwrQkFBK0I7SUFDL0IscUNBQXFDO0lBQ3JDLG9CQUFvQjtJQUNwQixzQkFBc0I7O0VBRXhCO0dBQ0M7OztNQUdHO0dBQ0o7SUFDRSxZQUFZO0VBQ2Q7R0FDQTtJQUNFLHFCQUFxQjtJQUNyQixXQUFXO0lBQ1gsaUJBQWlCO0lBQ2pCLFVBQVU7SUFDVixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIsNkNBQTZDO0VBQy9DO0dBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLFdBQVc7RUFDYjtHQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixVQUFVO0lBQ1YsZUFBZTs7RUFFakI7R0FDQTtJQUNFLGFBQWE7RUFDZjtHQUVBO01BQ0ksb0JBQW9CO0VBQ3hCO0dBQ0E7SUFDRSxnQkFBZ0I7RUFDbEI7R0FFQTs7TUFFSSx1QkFBaUI7O2NBQWpCLGlCQUFpQjtNQUNqQix5QkFBOEI7Y0FBOUIsOEJBQThCO0VBQ2xDIiwiZmlsZSI6InNyYy9hcHAvcXVpei1sb2JieS9xdWl6LWxvYmJ5LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJtYXQtY2FyZC10aXRsZSB7XHJcbiAgICAvKiBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAwOyAqL1xyXG4gICBcclxuICAgIGZvbnQ6aXRhbGljIDMwcHggR2VvcmdpYSwgc2VyaWY7XHJcbiAgICAvKiBmb250LXNpemU6IDcwJTsgbGluZS1oZWlnaHQ6ODAlOyAqL1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTgwcHg7XHJcbiAgICAvKiBtYXJnaW4tbGVmdDoyMHB4OyAqL1xyXG4gICAgXHJcbiAgfVxyXG4gICAvKiBwIHtcclxuICAgIG1hcmdpbjogMC44ZW0gMDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgfSAgKi9cclxuICAuY2FyZHMtc2V0IHtcclxuICAgIGZvbnQtc2l6ZTogMDtcclxuICB9XHJcbiAgLmNhcmQge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbWFyZ2luOiA4cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogNjBweDtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBtaW4taGVpZ2h0OiAyODBweDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICBib3gtc2hhZG93OiAwIDEycHggMTVweCAwIHJnYmEoMCwgMCwgMCwgMC4yNCk7XHJcbiAgfVxyXG4gIC5jYXJkIC5oZWFkaW5nIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGhlaWdodDogMjUwcHg7XHJcbiAgICBjb2xvcjpibGFjaztcclxuICB9XHJcbiAgLmNhcmQgLmhlYWRpbmcgaDEge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAxNnB4O1xyXG4gICAgbGVmdDogMTZweDtcclxuICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgIFxyXG4gIH1cclxuICAuY2FyZCAuY29udGVudCB7XHJcbiAgICBwYWRkaW5nOiAxNnB4O1xyXG4gIH1cclxuXHJcbiAgbWF0LWNhcmQtYWN0aW9uc3tcclxuICAgICAgYWxpZ24tY29udGVudDogcmlnaHQ7XHJcbiAgfVxyXG4gIC5leGFtcGxlLXN0cmV0Y2hlZC10YWJzIHtcclxuICAgIG1heC13aWR0aDogODAwcHg7XHJcbiAgfVxyXG5cclxuICBtYXQtdGFiLWdyb3Vwe1xyXG4gICAgICBcclxuICAgICAgYWxpZ24taXRlbXM6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB9XHJcblxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/quiz-lobby/quiz-lobby.component.ts":
/*!****************************************************!*\
  !*** ./src/app/quiz-lobby/quiz-lobby.component.ts ***!
  \****************************************************/
/*! exports provided: QuizLobbyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuizLobbyComponent", function() { return QuizLobbyComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_quiz_lobby_quiz_lobby_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/quiz-lobby/quiz-lobby.service */ "./src/app/quiz-lobby/quiz-lobby.service.ts");
/* harmony import */ var src_app_test_window_post_user_response_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/test-window/post-user-response.service */ "./src/app/test-window/post-user-response.service.ts");
/* harmony import */ var src_app_test_window_fetch_question_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/test-window/fetch-question.service */ "./src/app/test-window/fetch-question.service.ts");






let QuizLobbyComponent = class QuizLobbyComponent {
    constructor(quizLobbyService, route, router, postUserResponse, fetchUserInfoService) {
        this.quizLobbyService = quizLobbyService;
        this.route = route;
        this.router = router;
        this.postUserResponse = postUserResponse;
        this.fetchUserInfoService = fetchUserInfoService;
        this.questionIdArray = [];
        this.tempQuestionIdArray = [];
        this.sendQuestionArray = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.sendQuizDuration = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.sendQuizName = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.sendQuizPlaySubmissionId = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ngOnInit() {
        // const id = this.route.snapshot.paramMap.get('id');
        // this.quizCode = id;
        //*--------Below *ParamMap observable* is the substitute of SNAPSHOT approach.
        this.route.paramMap.subscribe((params) => {
            let id = params.get('id');
            this.quizCode = id;
        });
        const url = `quiz/quizzes/${this.quizCode}`;
        this.quizLobbyService.getQuizLobbyInfo(url).subscribe(res => {
            this.quiz = res;
            console.log("quiz fetched for quiz lobby-->", this.quiz);
            this.questionIds = this.quiz.questionId;
            console.log("Question Ids in quizlobby--", this.questionIds);
            this.shuffleQuestions(this.questionIds);
            // console.log("shuffled questionArray-->", this.shuffleQuestions(this.questionIds));
        });
        const loadUserUrl = `user/userprofile`;
        this.fetchUserInfoService.loadUserProfile(loadUserUrl).subscribe(res => {
            this.psid = res.psid;
            this.userMail = res.email;
            console.log("user psid-------->>>>", this.psid);
            console.log("user-mail---> ", this.userMail);
        });
    }
    shuffleQuestions(tempQuestionIdArray) {
        var currentIndex = tempQuestionIdArray.length, temporaryValue, randomIndex;
        //while there is element to shuffle..
        while (0 != currentIndex) {
            //Pick a random element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            //And swap it with the current element..
            temporaryValue = tempQuestionIdArray[currentIndex];
            tempQuestionIdArray[currentIndex] = tempQuestionIdArray[randomIndex];
            tempQuestionIdArray[randomIndex] = temporaryValue;
        }
        this.questionIdArray = tempQuestionIdArray;
        console.log("in ShuffleMethod====>", this.questionIdArray);
    }
    startQuiz() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            //creating a quiz submission when user clicks on Play Quiz button.
            const postSubmissionUrl = `quizplay/quizsubmissions/${this.psid}/${this.quizCode}`;
            yield this.postUserResponse.createUserSubmission(postSubmissionUrl).subscribe(res => {
                this.tempQuizSubmissionData = res;
                this.quizSubmissionId = this.tempQuizSubmissionData.result;
                this.quizLobbyService.sendQuizPlaySubmissionId(this.quizSubmissionId);
                console.log("quiz Submission details--:::--> ", this.quizSubmissionId);
                console.log("IMPORTANT");
            });
            //passing the qestionId's, quizName and the duration of quiz to the testWindow component.
            yield console.log("first await...++++++++>>>>>>>>>>>>>");
            yield this.quizLobbyService.sendQuestionArray(this.questionIdArray);
            yield this.quizLobbyService.sendQuizDuration(this.quiz.maxDurationMinutes);
            yield this.quizLobbyService.sendQuizName(this.quiz.quizName);
            console.log("on start click (quizlobby.ts) -- >", this.questionIdArray);
            this.router.navigate(['/testwindow', this.quizCode]);
        });
    }
    onCancel() {
        this.router.navigate(['/dashboard']);
    }
};
QuizLobbyComponent.ctorParameters = () => [
    { type: src_app_quiz_lobby_quiz_lobby_service__WEBPACK_IMPORTED_MODULE_3__["QuizLobbyService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_test_window_post_user_response_service__WEBPACK_IMPORTED_MODULE_4__["PostUserResponseService"] },
    { type: src_app_test_window_fetch_question_service__WEBPACK_IMPORTED_MODULE_5__["FetchQuestionService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], QuizLobbyComponent.prototype, "sendQuestionArray", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], QuizLobbyComponent.prototype, "sendQuizDuration", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], QuizLobbyComponent.prototype, "sendQuizName", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], QuizLobbyComponent.prototype, "sendQuizPlaySubmissionId", void 0);
QuizLobbyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-quiz-lobby',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./quiz-lobby.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/quiz-lobby/quiz-lobby.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./quiz-lobby.component.css */ "./src/app/quiz-lobby/quiz-lobby.component.css")).default]
    })
], QuizLobbyComponent);



/***/ }),

/***/ "./src/app/quiz-lobby/quiz-lobby.service.ts":
/*!**************************************************!*\
  !*** ./src/app/quiz-lobby/quiz-lobby.service.ts ***!
  \**************************************************/
/*! exports provided: QuizLobbyService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuizLobbyService", function() { return QuizLobbyService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/internal/BehaviorSubject */ "./node_modules/rxjs/internal/BehaviorSubject.js");
/* harmony import */ var rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_3__);




let QuizLobbyService = class QuizLobbyService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        //to pass question Array to the test window componet
        this.listener = new rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]([]);
        this.timeListener = new rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]('');
        this.quizNameListener = new rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]('');
        this.quizPlaySubmissionIdListener = new rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]('');
    }
    //To fetch the info of any particular quiz for quiz lobby..
    getQuizLobbyInfo(url) {
        return this.httpClient.get(url);
    }
    startQuiz(url) {
        return this.httpClient.get(url);
    }
    sendQuestionArray(questionIdArray) {
        this.listener.next(questionIdArray);
    }
    getQuestionArray() {
        return this.listener;
    }
    sendQuizDuration(quizDuration) {
        this.timeListener.next(quizDuration);
    }
    getQuizDuration() {
        return this.timeListener;
    }
    sendQuizName(quizName) {
        this.quizNameListener.next(quizName);
    }
    getQuizName() {
        return this.quizNameListener;
    }
    sendQuizPlaySubmissionId(quizSubmissionId) {
        console.log("submissionId in service send-->", quizSubmissionId);
        this.quizPlaySubmissionIdListener.next(quizSubmissionId);
    }
    getQuizPlaySubmissionId() {
        console.log("submission id in service get", this.quizPlaySubmissionIdListener);
        return this.quizPlaySubmissionIdListener;
    }
};
QuizLobbyService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
QuizLobbyService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], QuizLobbyService);



/***/ }),

/***/ "./src/app/registration/registration.component.css":
/*!*********************************************************!*\
  !*** ./src/app/registration/registration.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\r\n    display: -webkit-box;\r\n    display: flex;\r\n    -webkit-box-pack: center;\r\n            justify-content: center;\r\n    margin: 100px 0px;\r\n  }\r\n\r\n  .button {\r\n    display: -webkit-box;\r\n    display: flex;\r\n    -webkit-box-pack: end;\r\n            justify-content: flex-end;\r\n  }\r\n\r\n  .example-container {\r\n    display: -webkit-box;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n            flex-direction: column;\r\n    width: 100%;\r\n    height: 250px;\r\n  }\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  .example-right-align {\r\n    text-align: right;\r\n  }\r\n\r\n  input.example-right-align::-webkit-outer-spin-button,\r\n  input.example-right-align::-webkit-inner-spin-button {\r\n    display: none;\r\n  }\r\n\r\n  input.example-right-align {\r\n    -moz-appearance: textfield;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0cmF0aW9uL3JlZ2lzdHJhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksb0JBQWE7SUFBYixhQUFhO0lBQ2Isd0JBQXVCO1lBQXZCLHVCQUF1QjtJQUN2QixpQkFBaUI7RUFDbkI7O0VBRUE7SUFDRSxvQkFBYTtJQUFiLGFBQWE7SUFDYixxQkFBeUI7WUFBekIseUJBQXlCO0VBQzNCOztFQUVBO0lBQ0Usb0JBQWE7SUFBYixhQUFhO0lBQ2IsNEJBQXNCO0lBQXRCLDZCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIsV0FBVztJQUNYLGFBQWE7RUFDZjs7RUFFQTtJQUNFLFdBQVc7RUFDYjs7RUFFQTtJQUNFLGlCQUFpQjtFQUNuQjs7RUFFQTs7SUFFRSxhQUFhO0VBQ2Y7O0VBRUE7SUFDRSwwQkFBMEI7RUFDNUIiLCJmaWxlIjoic3JjL2FwcC9yZWdpc3RyYXRpb24vcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDEwMHB4IDBweDtcclxuICB9XHJcblxyXG4gIC5idXR0b24ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgfVxyXG5cclxuICAuZXhhbXBsZS1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMjUwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLXJpZ2h0LWFsaWduIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gIH1cclxuICBcclxuICBpbnB1dC5leGFtcGxlLXJpZ2h0LWFsaWduOjotd2Via2l0LW91dGVyLXNwaW4tYnV0dG9uLFxyXG4gIGlucHV0LmV4YW1wbGUtcmlnaHQtYWxpZ246Oi13ZWJraXQtaW5uZXItc3Bpbi1idXR0b24ge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcbiAgXHJcbiAgaW5wdXQuZXhhbXBsZS1yaWdodC1hbGlnbiB7XHJcbiAgICAtbW96LWFwcGVhcmFuY2U6IHRleHRmaWVsZDtcclxuICB9Il19 */");

/***/ }),

/***/ "./src/app/registration/registration.component.ts":
/*!********************************************************!*\
  !*** ./src/app/registration/registration.component.ts ***!
  \********************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");





let RegistrationComponent = class RegistrationComponent {
    constructor(router, dialogRef) {
        this.router = router;
        this.dialogRef = dialogRef;
        this.hide = true;
        this.email = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]);
    }
    getErrorMessage() {
        return this.email.hasError('required') ? 'You must enter a value' :
            this.email.hasError('email') ? 'Not a valid email' :
                '';
    }
    ngOnInit() {
    }
    submit() {
        this.msg = "Successfully Registered, Kindly login to continue.";
        this.router.navigateByUrl('');
        this.dialogRef.close();
    }
};
RegistrationComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"] }
];
RegistrationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-registration',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./registration.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/registration/registration.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./registration.component.css */ "./src/app/registration/registration.component.css")).default]
    })
], RegistrationComponent);



/***/ }),

/***/ "./src/app/result/fetch-result.service.ts":
/*!************************************************!*\
  !*** ./src/app/result/fetch-result.service.ts ***!
  \************************************************/
/*! exports provided: FetchResultService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FetchResultService", function() { return FetchResultService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let FetchResultService = class FetchResultService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getQuizResult(url) {
        return this.httpClient.get(url);
    }
};
FetchResultService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
FetchResultService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], FetchResultService);



/***/ }),

/***/ "./src/app/result/result.component.css":
/*!*********************************************!*\
  !*** ./src/app/result/result.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Jlc3VsdC9yZXN1bHQuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/result/result.component.ts":
/*!********************************************!*\
  !*** ./src/app/result/result.component.ts ***!
  \********************************************/
/*! exports provided: ResultComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultComponent", function() { return ResultComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_result_fetch_result_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/result/fetch-result.service */ "./src/app/result/fetch-result.service.ts");
/* harmony import */ var src_app_test_window_fetch_question_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/test-window/fetch-question.service */ "./src/app/test-window/fetch-question.service.ts");





let ResultComponent = class ResultComponent {
    constructor(route, fetchResultService, fetchUserInfoService) {
        this.route = route;
        this.fetchResultService = fetchResultService;
        this.fetchUserInfoService = fetchUserInfoService;
    }
    ngOnInit() {
        console.log("NG_ONINIT in Result window.....::::::::::: ");
        this.route.paramMap.subscribe((params) => {
            let id = params.get('id');
            this.quizSubmissionId = id;
            console.log("in result view-->", this.quizSubmissionId);
        });
        console.log("QUIZ-SUbmission id in result view ouside subscribe-->", this.quizSubmissionId);
        const url1 = `quizplay/quizsubmissions/${this.quizSubmissionId}`;
        this.fetchResultService.getQuizResult(url1).subscribe(res => {
            this.quizResult = res;
            this.quizSubmissionId = this.quizResult.submissionId;
            this.totalMarks = this.quizResult.totalPointsScored;
        });
        const loadUserUrl = `user/userprofile`;
        this.fetchUserInfoService.loadUserProfile(loadUserUrl).subscribe(res => {
            this.psid = res.psid;
            this.userMail = res.email;
            console.log("user psid>>>>", this.psid);
            console.log("user-mail---> ", this.userMail);
        });
    }
};
ResultComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_result_fetch_result_service__WEBPACK_IMPORTED_MODULE_3__["FetchResultService"] },
    { type: src_app_test_window_fetch_question_service__WEBPACK_IMPORTED_MODULE_4__["FetchQuestionService"] }
];
ResultComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-result',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./result.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/result/result.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./result.component.css */ "./src/app/result/result.component.css")).default]
    })
], ResultComponent);



/***/ }),

/***/ "./src/app/san-carousel/san-carousel.component.css":
/*!*********************************************************!*\
  !*** ./src/app/san-carousel/san-carousel.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* .picsum-img-wrapper{\r\n    width: 100%;\r\n    height: 30vh;\r\n} */\r\n\r\nngb-carousel .picsum-img-wrapper {\r\n    position: relative;\r\n    height: 0;\r\n    padding-top: 25%; /* Keep ratio for 900x500 images */\r\n  }\r\n\r\nngb-carousel .picsum-img-wrapper>img {\r\n    position: absolute;\r\n    width: 100vw;\r\n    height: 100%;\r\n    margin-top: 10px;\r\n    margin-left: 20px;\r\n    margin-right: 50px;\r\n    top: 0;\r\n    left: 0;\r\n    bottom: 0;\r\n    right: 0;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2FuLWNhcm91c2VsL3Nhbi1jYXJvdXNlbC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7R0FHRzs7QUFFSDtJQUNJLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsZ0JBQWdCLEVBQUUsa0NBQWtDO0VBQ3REOztBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLE9BQU87SUFDUCxTQUFTO0lBQ1QsUUFBUTtFQUNWIiwiZmlsZSI6InNyYy9hcHAvc2FuLWNhcm91c2VsL3Nhbi1jYXJvdXNlbC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLnBpY3N1bS1pbWctd3JhcHBlcntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAzMHZoO1xyXG59ICovXHJcblxyXG5uZ2ItY2Fyb3VzZWwgLnBpY3N1bS1pbWctd3JhcHBlciB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBoZWlnaHQ6IDA7XHJcbiAgICBwYWRkaW5nLXRvcDogMjUlOyAvKiBLZWVwIHJhdGlvIGZvciA5MDB4NTAwIGltYWdlcyAqL1xyXG4gIH1cclxuICBcclxuICBuZ2ItY2Fyb3VzZWwgLnBpY3N1bS1pbWctd3JhcHBlcj5pbWcge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgd2lkdGg6IDEwMHZ3O1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/san-carousel/san-carousel.component.ts":
/*!********************************************************!*\
  !*** ./src/app/san-carousel/san-carousel.component.ts ***!
  \********************************************************/
/*! exports provided: SanCarouselComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SanCarouselComponent", function() { return SanCarouselComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SanCarouselComponent = class SanCarouselComponent {
    constructor() {
        // images = [62, 83, 466, 965, 982, 1043, 738].map((n) => `https://picsum.photos/id/${n}/900/500`);
        this.images = ["https://picsum.photos/200/300",
            "https://picsum.photos/200/301",
            "https://picsum.photos/200/302"
        ];
    }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('carousel', { static: true })
], SanCarouselComponent.prototype, "carousel", void 0);
SanCarouselComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-san-carousel',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./san-carousel.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/san-carousel/san-carousel.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./san-carousel.component.css */ "./src/app/san-carousel/san-carousel.component.css")).default]
    })
], SanCarouselComponent);



/***/ }),

/***/ "./src/app/shared-component/card/card.component.css":
/*!**********************************************************!*\
  !*** ./src/app/shared-component/card/card.component.css ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".quiz-card {\r\n    max-width: 300px;\r\n    min-width: 300px;\r\n    padding: 0px;\r\n    margin: 10px;\r\n    border: 1em;\r\n}\r\n\r\n.subtitle {\r\n    width: 85%;\r\n}\r\n\r\n.quizTitle {\r\n    margin-left: 17px;\r\n}\r\n\r\n.image {\r\n    display: -webkit-box;\r\n    display: flex;\r\n    margin-left: -20px;\r\n}\r\n\r\n.mat-button {\r\n    margin-top: 10px;\r\n}\r\n\r\n.avatar {\r\n    vertical-align: middle;\r\n    width: 55px;\r\n    height: 55px;\r\n    border-radius: 50%;\r\n}\r\n\r\n.header-color {\r\n    margin-top: 3px;\r\n    padding-top: 10px;\r\n}\r\n\r\n.card-content {\r\n    padding-left: 10px;\r\n    padding-right: 10px;\r\n    margin-right: 7px;\r\n    margin-top: 25px;\r\n}\r\n\r\n.card-icons {\r\n    margin-top: 12px;\r\n    margin-bottom: 12px;\r\n    padding-right: 7px;\r\n}\r\n\r\n.description-view {\r\n    max-width: 300px;\r\n}\r\n\r\n.icon-flex {\r\n    display: -webkit-box;\r\n    display: flex;\r\n    -webkit-box-pack: space-evenly;\r\n            justify-content: space-evenly;\r\n}\r\n\r\n.icon-space {\r\n    size: 30px;\r\n    font: 30px;\r\n    padding-top: 5px;\r\n    padding-left: 5px;\r\n}\r\n\r\n.avatar-icon {\r\n    display: -webkit-box;\r\n    display: flex;\r\n    -webkit-box-pack: center;\r\n            justify-content: center;\r\n}\r\n\r\n.avatar-icon1 {\r\n    display: -webkit-box;\r\n    display: flex;\r\n    -webkit-box-pack: center;\r\n            justify-content: center;\r\n    margin-left: -10%;\r\n}\r\n\r\n.avatar-count {\r\n    padding-top: 16px;\r\n    padding-left: 4px;\r\n    font-size: 13px;\r\n}\r\n\r\n.avtar {\r\n    padding-top: 20px;\r\n    padding-left: 2px;\r\n    margin-left: 15px;\r\n    margin-top: 1em;\r\n    background-color: #b2c7f5;\r\n    font-weight: bold;\r\n    text-align: center;\r\n    width: 16%;\r\n}\r\n\r\n.avatar-icons {\r\n    display: inline-block;\r\n    -webkit-transform: scale(1, 1);\r\n            transform: scale(1, 1);\r\n    padding-left: 35px;\r\n    /* padding-bottom: 10px; */\r\n}\r\n\r\n.avatar-i {\r\n    margin-left: -25px;\r\n    position: relative;\r\n    display: inline-block;\r\n    border: 1px solid #fff;\r\n    border-radius: 50%;\r\n    overflow: hidden;\r\n    width: 2.4em;\r\n    height: 2.4em;\r\n}\r\n\r\n.avatar-i img {\r\n    width: 50px;\r\n    height: 50px;\r\n    -webkit-transform: scale(1, 1);\r\n            transform: scale(1, 1);\r\n}\r\n\r\n.example-header-image {\r\n    max-width: 70px;\r\n    max-height: 70px;\r\n    min-width: 70px;\r\n    min-height: 70px;\r\n    background-image: url('https://www.seekclipart.com/clipng/middle/87-876167_avatar-michael-jordan-jersey-clip-art-michael-jordan.png');\r\n    background-size: cover;\r\n}\r\n\r\n.nav-icon {\r\n    color: rgb(117, 117, 117);\r\n}\r\n\r\n.fav-icon {\r\n    color: rgb(117, 117, 117);\r\n    font-size: 28px;\r\n    margin-left: 10px;\r\n    margin-bottom: 13px;\r\n}\r\n\r\n.example-card1 {\r\n    max-width: 400px;\r\n  }\r\n\r\n.example-header-image1 {\r\n    background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\r\n    background-size: cover;\r\n  }\r\n\r\n.dashboar-carousel {\r\n      max-width: auto;\r\n      max-height: 400px;\r\n  }\r\n\r\n.quiz-Card-1 {\r\n      border-radius: 5%;\r\n      color: aliceblue;\r\n      width: 250px;\r\n      height: 130px;\r\n      cursor: pointer;\r\n      background:linear-gradient(135deg, #7117ea 0%,#e75e62 100%);\r\n  }\r\n\r\n.card-footer1 {\r\n    width: 250px;\r\n    height: 20px;\r\n    background: turquoise;\r\n  }\r\n\r\n.quiz-Card-1:hover {\r\n    cursor: pointer;\r\n    -webkit-transform: scale(1);\r\n\ttransform: scale(1.02);\r\n    background-color:grey;\r\n  }\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkLWNvbXBvbmVudC9jYXJkL2NhcmQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLFlBQVk7SUFDWixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxvQkFBYTtJQUFiLGFBQWE7SUFDYixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxzQkFBc0I7SUFDdEIsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxvQkFBYTtJQUFiLGFBQWE7SUFDYiw4QkFBNkI7WUFBN0IsNkJBQTZCO0FBQ2pDOztBQUVBO0lBQ0ksVUFBVTtJQUNWLFVBQVU7SUFDVixnQkFBZ0I7SUFDaEIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksb0JBQWE7SUFBYixhQUFhO0lBQ2Isd0JBQXVCO1lBQXZCLHVCQUF1QjtBQUMzQjs7QUFFQTtJQUNJLG9CQUFhO0lBQWIsYUFBYTtJQUNiLHdCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLGlCQUFpQjtJQUNqQixlQUFlO0FBQ25COztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLGlCQUFpQjtJQUNqQixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLFVBQVU7QUFDZDs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQiw4QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLGtCQUFrQjtJQUNsQiwwQkFBMEI7QUFDOUI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixzQkFBc0I7SUFDdEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixZQUFZO0lBQ1osYUFBYTtBQUNqQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0lBQ1osOEJBQXNCO1lBQXRCLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixxSUFBcUk7SUFDckksc0JBQXNCO0FBQzFCOztBQUVBO0lBQ0kseUJBQXlCO0FBQzdCOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsbUJBQW1CO0FBQ3ZCOztBQU1BO0lBQ0ksZ0JBQWdCO0VBQ2xCOztBQUVBO0lBQ0UsbUZBQW1GO0lBQ25GLHNCQUFzQjtFQUN4Qjs7QUFHQTtNQUNJLGVBQWU7TUFDZixpQkFBaUI7RUFDckI7O0FBRUE7TUFDSSxpQkFBaUI7TUFDakIsZ0JBQWdCO01BQ2hCLFlBQVk7TUFDWixhQUFhO01BQ2IsZUFBZTtNQUNmLDJEQUEyRDtFQUMvRDs7QUFFQTtJQUNFLFlBQVk7SUFDWixZQUFZO0lBQ1oscUJBQXFCO0VBQ3ZCOztBQUVBO0lBQ0UsZUFBZTtJQUNmLDJCQUEyQjtDQUk5QixzQkFBc0I7SUFDbkIscUJBQXFCO0VBQ3ZCIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkLWNvbXBvbmVudC9jYXJkL2NhcmQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5xdWl6LWNhcmQge1xyXG4gICAgbWF4LXdpZHRoOiAzMDBweDtcclxuICAgIG1pbi13aWR0aDogMzAwcHg7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICBtYXJnaW46IDEwcHg7XHJcbiAgICBib3JkZXI6IDFlbTtcclxufVxyXG5cclxuLnN1YnRpdGxlIHtcclxuICAgIHdpZHRoOiA4NSU7XHJcbn1cclxuXHJcbi5xdWl6VGl0bGUge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE3cHg7XHJcbn1cclxuXHJcbi5pbWFnZSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0yMHB4O1xyXG59XHJcblxyXG4ubWF0LWJ1dHRvbiB7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uYXZhdGFyIHtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICB3aWR0aDogNTVweDtcclxuICAgIGhlaWdodDogNTVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxufVxyXG5cclxuLmhlYWRlci1jb2xvciB7XHJcbiAgICBtYXJnaW4tdG9wOiAzcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxufVxyXG5cclxuLmNhcmQtY29udGVudCB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA3cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAyNXB4O1xyXG59XHJcblxyXG4uY2FyZC1pY29ucyB7XHJcbiAgICBtYXJnaW4tdG9wOiAxMnB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTJweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDdweDtcclxufVxyXG5cclxuLmRlc2NyaXB0aW9uLXZpZXcge1xyXG4gICAgbWF4LXdpZHRoOiAzMDBweDtcclxufVxyXG5cclxuLmljb24tZmxleCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XHJcbn1cclxuXHJcbi5pY29uLXNwYWNlIHtcclxuICAgIHNpemU6IDMwcHg7XHJcbiAgICBmb250OiAzMHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG59XHJcblxyXG4uYXZhdGFyLWljb24ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4uYXZhdGFyLWljb24xIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMTAlO1xyXG59XHJcblxyXG4uYXZhdGFyLWNvdW50IHtcclxuICAgIHBhZGRpbmctdG9wOiAxNnB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiA0cHg7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbi5hdnRhciB7XHJcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICAgIHBhZGRpbmctbGVmdDogMnB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxZW07XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjJjN2Y1O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTYlO1xyXG59XHJcblxyXG4uYXZhdGFyLWljb25zIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHRyYW5zZm9ybTogc2NhbGUoMSwgMSk7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDM1cHg7XHJcbiAgICAvKiBwYWRkaW5nLWJvdHRvbTogMTBweDsgKi9cclxufVxyXG5cclxuLmF2YXRhci1pIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMjVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgd2lkdGg6IDIuNGVtO1xyXG4gICAgaGVpZ2h0OiAyLjRlbTtcclxufVxyXG5cclxuLmF2YXRhci1pIGltZyB7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIHRyYW5zZm9ybTogc2NhbGUoMSwgMSk7XHJcbn1cclxuXHJcbi5leGFtcGxlLWhlYWRlci1pbWFnZSB7XHJcbiAgICBtYXgtd2lkdGg6IDcwcHg7XHJcbiAgICBtYXgtaGVpZ2h0OiA3MHB4O1xyXG4gICAgbWluLXdpZHRoOiA3MHB4O1xyXG4gICAgbWluLWhlaWdodDogNzBweDtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnaHR0cHM6Ly93d3cuc2Vla2NsaXBhcnQuY29tL2NsaXBuZy9taWRkbGUvODctODc2MTY3X2F2YXRhci1taWNoYWVsLWpvcmRhbi1qZXJzZXktY2xpcC1hcnQtbWljaGFlbC1qb3JkYW4ucG5nJyk7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcblxyXG4ubmF2LWljb24ge1xyXG4gICAgY29sb3I6IHJnYigxMTcsIDExNywgMTE3KTtcclxufVxyXG5cclxuLmZhdi1pY29uIHtcclxuICAgIGNvbG9yOiByZ2IoMTE3LCAxMTcsIDExNyk7XHJcbiAgICBmb250LXNpemU6IDI4cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDEzcHg7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG4uZXhhbXBsZS1jYXJkMSB7XHJcbiAgICBtYXgtd2lkdGg6IDQwMHB4O1xyXG4gIH1cclxuICBcclxuICAuZXhhbXBsZS1oZWFkZXItaW1hZ2UxIHtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnaHR0cHM6Ly9tYXRlcmlhbC5hbmd1bGFyLmlvL2Fzc2V0cy9pbWcvZXhhbXBsZXMvc2hpYmExLmpwZycpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICB9XHJcblxyXG5cclxuICAuZGFzaGJvYXItY2Fyb3VzZWwge1xyXG4gICAgICBtYXgtd2lkdGg6IGF1dG87XHJcbiAgICAgIG1heC1oZWlnaHQ6IDQwMHB4O1xyXG4gIH1cclxuXHJcbiAgLnF1aXotQ2FyZC0xIHtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNSU7XHJcbiAgICAgIGNvbG9yOiBhbGljZWJsdWU7XHJcbiAgICAgIHdpZHRoOiAyNTBweDtcclxuICAgICAgaGVpZ2h0OiAxMzBweDtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICBiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCgxMzVkZWcsICM3MTE3ZWEgMCUsI2U3NWU2MiAxMDAlKTtcclxuICB9XHJcblxyXG4gIC5jYXJkLWZvb3RlcjEge1xyXG4gICAgd2lkdGg6IDI1MHB4O1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgYmFja2dyb3VuZDogdHVycXVvaXNlO1xyXG4gIH1cclxuXHJcbiAgLnF1aXotQ2FyZC0xOmhvdmVyIHtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuXHQtbW96LXRyYW5zZm9ybTogc2NhbGUoMSk7XHJcblx0LW8tdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuXHQtbXMtdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuXHR0cmFuc2Zvcm06IHNjYWxlKDEuMDIpO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjpncmV5O1xyXG4gIH1cclxuXHJcblxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/shared-component/card/card.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/shared-component/card/card.component.ts ***!
  \*********************************************************/
/*! exports provided: CardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardComponent", function() { return CardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let CardComponent = class CardComponent {
    constructor(router) {
        this.router = router;
        this.fav = true;
    }
    ngOnInit() {
    }
    addToFavourite($quizCode) {
    }
    onSelect() {
        this.router.navigate(['/quizlobby', this.quiz.quizCode]);
    }
};
CardComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], CardComponent.prototype, "quiz", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], CardComponent.prototype, "componentName", void 0);
CardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-card',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./card.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared-component/card/card.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./card.component.css */ "./src/app/shared-component/card/card.component.css")).default]
    })
], CardComponent);



/***/ }),

/***/ "./src/app/shared-component/header/header.component.css":
/*!**************************************************************!*\
  !*** ./src/app/shared-component/header/header.component.css ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("header {\r\n    width: 100vw;\r\n    height: 10vh;\r\n    background: black;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkLWNvbXBvbmVudC9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0lBQ1osWUFBWTtJQUNaLGlCQUFpQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC1jb21wb25lbnQvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaGVhZGVyIHtcclxuICAgIHdpZHRoOiAxMDB2dztcclxuICAgIGhlaWdodDogMTB2aDtcclxuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/shared-component/header/header.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/shared-component/header/header.component.ts ***!
  \*************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HeaderComponent = class HeaderComponent {
    constructor() { }
    ngOnInit() {
    }
};
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared-component/header/header.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./header.component.css */ "./src/app/shared-component/header/header.component.css")).default]
    })
], HeaderComponent);



/***/ }),

/***/ "./src/app/shared-component/shared-component.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/shared-component/shared-component.module.ts ***!
  \*************************************************************/
/*! exports provided: SharedComponentModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedComponentModule", function() { return SharedComponentModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _card_card_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./card/card.component */ "./src/app/shared-component/card/card.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm2015/icon.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/esm2015/chips.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm2015/tooltip.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm2015/card.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./header/header.component */ "./src/app/shared-component/header/header.component.ts");














let SharedComponentModule = class SharedComponentModule {
};
SharedComponentModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_card_card_component__WEBPACK_IMPORTED_MODULE_3__["CardComponent"],
            _header_header_component__WEBPACK_IMPORTED_MODULE_12__["HeaderComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"],
            _angular_material_card__WEBPACK_IMPORTED_MODULE_10__["MatCardModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"],
            _angular_material_chips__WEBPACK_IMPORTED_MODULE_8__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDividerModule"],
            _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_9__["MatTooltipModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"]
        ],
        exports: [
            _card_card_component__WEBPACK_IMPORTED_MODULE_3__["CardComponent"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"],
            _angular_material_chips__WEBPACK_IMPORTED_MODULE_8__["MatChipsModule"],
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"],
            _angular_material_card__WEBPACK_IMPORTED_MODULE_10__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDividerModule"],
            _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_9__["MatTooltipModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"],
            _header_header_component__WEBPACK_IMPORTED_MODULE_12__["HeaderComponent"]
        ]
    })
], SharedComponentModule);



/***/ }),

/***/ "./src/app/test-window/fetch-question.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/test-window/fetch-question.service.ts ***!
  \*******************************************************/
/*! exports provided: FetchQuestionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FetchQuestionService", function() { return FetchQuestionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let FetchQuestionService = class FetchQuestionService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    fetchQuestionById(url) {
        return this.httpClient.get(url);
    }
    loadUserProfile(url) {
        return this.httpClient.get(url);
    }
};
FetchQuestionService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
FetchQuestionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], FetchQuestionService);



/***/ }),

/***/ "./src/app/test-window/post-user-response.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/test-window/post-user-response.service.ts ***!
  \***********************************************************/
/*! exports provided: PostUserResponseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostUserResponseService", function() { return PostUserResponseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let PostUserResponseService = class PostUserResponseService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    createUserSubmission(url) {
        return this.httpClient.post(url, null);
    }
    postUserResponse(url) {
        return this.httpClient.post(url, null);
    }
};
PostUserResponseService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
PostUserResponseService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], PostUserResponseService);



/***/ }),

/***/ "./src/app/test-window/test-window.component.css":
/*!*******************************************************!*\
  !*** ./src/app/test-window/test-window.component.css ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("h1 {\r\n    margin: 0;\r\n    padding: 0;\r\n  }\r\n  p {\r\n    margin: 0.8em 0;\r\n    padding: 0;\r\n  }\r\n  .cards-set {\r\n    font-size: 0;\r\n    margin-top: 60px;\r\n     margin-left: 200px; \r\n   /* margin-right: 150px; */\r\n  }\r\n  .card {\r\n    display: inline-block;\r\n    margin: 8px;\r\n    width: 80%;\r\n    -webkit-box-align: center;\r\n            align-items: center;\r\n    -webkit-box-pack: center;\r\n            justify-content: center;\r\n    font-size: 20px;\r\n    background-color: #ffffff;\r\n    border-radius: 2px;\r\n    box-shadow: 0 12px 15px 0 rgba(0, 0, 0, 0.24);\r\n  }\r\n  .card .heading {\r\n    position: relative;\r\n    height: 350px;\r\n    color:black;\r\n  }\r\n  .card .heading h1 {\r\n    position: absolute;\r\n    bottom: 16px;\r\n    left: 16px;\r\n    font-size: 50px;\r\n  }\r\n  .card .content {\r\n    padding: 16px;\r\n  }\r\n  .example-radio-group {\r\n    display: -webkit-box;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n            flex-direction: column;\r\n    margin: 15px 0;\r\n  }\r\n  .example-radio-button {\r\n    margin: 5px;\r\n  }\r\n  \r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVzdC13aW5kb3cvdGVzdC13aW5kb3cuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFNBQVM7SUFDVCxVQUFVO0VBQ1o7RUFDQTtJQUNFLGVBQWU7SUFDZixVQUFVO0VBQ1o7RUFDQTtJQUNFLFlBQVk7SUFDWixnQkFBZ0I7S0FDZixrQkFBa0I7R0FDcEIseUJBQXlCO0VBQzFCO0VBQ0E7SUFDRSxxQkFBcUI7SUFDckIsV0FBVztJQUNYLFVBQVU7SUFDVix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLHdCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIsNkNBQTZDO0VBQy9DO0VBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLFdBQVc7RUFDYjtFQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixVQUFVO0lBQ1YsZUFBZTtFQUNqQjtFQUNBO0lBQ0UsYUFBYTtFQUNmO0VBRUE7SUFDRSxvQkFBYTtJQUFiLGFBQWE7SUFDYiw0QkFBc0I7SUFBdEIsNkJBQXNCO1lBQXRCLHNCQUFzQjtJQUN0QixjQUFjO0VBQ2hCO0VBRUE7SUFDRSxXQUFXO0VBQ2IiLCJmaWxlIjoic3JjL2FwcC90ZXN0LXdpbmRvdy90ZXN0LXdpbmRvdy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDEge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICB9XHJcbiAgcCB7XHJcbiAgICBtYXJnaW46IDAuOGVtIDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gIH1cclxuICAuY2FyZHMtc2V0IHtcclxuICAgIGZvbnQtc2l6ZTogMDtcclxuICAgIG1hcmdpbi10b3A6IDYwcHg7XHJcbiAgICAgbWFyZ2luLWxlZnQ6IDIwMHB4OyBcclxuICAgLyogbWFyZ2luLXJpZ2h0OiAxNTBweDsgKi9cclxuICB9XHJcbiAgLmNhcmQge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbWFyZ2luOiA4cHg7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgIGJveC1zaGFkb3c6IDAgMTJweCAxNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjI0KTtcclxuICB9XHJcbiAgLmNhcmQgLmhlYWRpbmcge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAzNTBweDtcclxuICAgIGNvbG9yOmJsYWNrO1xyXG4gIH1cclxuICAuY2FyZCAuaGVhZGluZyBoMSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IDE2cHg7XHJcbiAgICBsZWZ0OiAxNnB4O1xyXG4gICAgZm9udC1zaXplOiA1MHB4O1xyXG4gIH1cclxuICAuY2FyZCAuY29udGVudCB7XHJcbiAgICBwYWRkaW5nOiAxNnB4O1xyXG4gIH1cclxuXHJcbiAgLmV4YW1wbGUtcmFkaW8tZ3JvdXAge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBtYXJnaW46IDE1cHggMDtcclxuICB9XHJcbiAgXHJcbiAgLmV4YW1wbGUtcmFkaW8tYnV0dG9uIHtcclxuICAgIG1hcmdpbjogNXB4O1xyXG4gIH1cclxuICBcclxuIl19 */");

/***/ }),

/***/ "./src/app/test-window/test-window.component.ts":
/*!******************************************************!*\
  !*** ./src/app/test-window/test-window.component.ts ***!
  \******************************************************/
/*! exports provided: TestWindowComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestWindowComponent", function() { return TestWindowComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_quiz_lobby_quiz_lobby_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/quiz-lobby/quiz-lobby.service */ "./src/app/quiz-lobby/quiz-lobby.service.ts");
/* harmony import */ var src_app_test_window_fetch_question_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/test-window/fetch-question.service */ "./src/app/test-window/fetch-question.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_test_window_post_user_response_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/test-window/post-user-response.service */ "./src/app/test-window/post-user-response.service.ts");






let TestWindowComponent = class TestWindowComponent {
    constructor(quizLobbyService, fetchQuestionArrayService, postUserResponseService, router, route) {
        this.quizLobbyService = quizLobbyService;
        this.fetchQuestionArrayService = fetchQuestionArrayService;
        this.postUserResponseService = postUserResponseService;
        this.router = router;
        this.route = route;
        // favoriteSeason: string;
        // seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];
        this.questionIdArray = [];
        this.count = 1;
        this.lastQuestion = false;
        this.endTimmer = false;
    }
    // constructor(){}
    ngOnInit() {
        this.quizLobbyService.getQuestionArray().subscribe(data => {
            this.questionIdArray = data;
            this.totalNoOfQuestions = this.questionIdArray.length;
            console.log("total number of questions in the system ", this.totalNoOfQuestions);
            this.firstQuestionId = this.questionIdArray[0];
            console.log("Question array in test window...", this.questionIdArray);
            console.log("first questionId:---> ", this.questionIdArray[0]);
        });
        this.quizLobbyService.getQuizDuration().subscribe(quizDuration => {
            this.quizDuration = quizDuration;
            console.log("Quiz Duration ----> ", this.quizDuration);
        });
        this.quizLobbyService.getQuizName().subscribe(quizname => {
            this.quizName = quizname;
        });
        this.quizLobbyService.getQuizPlaySubmissionId().subscribe(submissionId => {
            this.quizPlaySubmissionId = submissionId;
            console.log("submission id in test window---:::-->", this.quizPlaySubmissionId);
        });
        this.route.paramMap.subscribe((params) => {
            let id = params.get('quizCode');
            this.quizCode = id;
            console.log("QUIZCODE____>", this.quizCode);
        });
        const urlfetchQuestionById = `ques/questions/${this.firstQuestionId}`;
        this.fetchQuestionArrayService.fetchQuestionById(urlfetchQuestionById).subscribe(res => {
            this.question = res;
            console.log("first question fetched---->", this.question);
        });
        const loadUserUrl = `user/userprofile`;
        this.fetchQuestionArrayService.loadUserProfile(loadUserUrl).subscribe(res => {
            this.psid = res.psid;
            this.userMail = res.email;
            console.log("user psid>>>>", this.psid);
            console.log("user-mail---> ", this.userMail);
        });
        this.startTimer();
    }
    onNextButtonClick() {
        // if(this.count < this.totalNoOfQuestions) {
        //   const urlfetchQuestionById = `santander-question-inventory/questions/${this.questionIdArray[this.count]}`;
        //   this.fetchQuestionArrayService.fetchQuestionById(urlfetchQuestionById).subscribe(res =>{
        //     this.question.push(res);
        //     console.log("Next question fetched---->", this.question);
        //     this.count ++;
        //   })
        // }
        if (this.count <= (this.totalNoOfQuestions - 1)) {
            const urlfetchQuestionById = `ques/questions/${this.questionIdArray[this.count]}`;
            this.fetchQuestionArrayService.fetchQuestionById(urlfetchQuestionById).subscribe(res => {
                this.question = res;
                console.log("Next question fetched---->", this.question);
                this.count++;
            });
            console.log("domain is:>>>>>>>>>>>>>>>>>>>>>>", this.question.domain);
            //posting the user selected option with the (question Id - 1) to evaluate
            const postUserSelectedOption = `quizplay/quizsubmissions/${this.psid}/${this.quizCode}/${this.questionIdArray[(this.count - 1)]}/${this.quizPlaySubmissionId}/${this.userSelectedOption}/${this.lastQuestion}/${this.question.domain}`;
            this.postUserResponseService.postUserResponse(postUserSelectedOption).subscribe(evaluatedResult => {
                this.evaluatedResult = evaluatedResult;
            });
            console.log("user selected option--> ", this.userSelectedOption);
        }
        else if (this.count == this.totalNoOfQuestions) {
            this.evaluatingLastQuestion();
        }
    }
    evaluatingLastQuestion() {
        // this.lastQuestion == true;
        console.log("2nd domian  ");
        const postUserSelectedOption = `quizplay/quizsubmissions/${this.psid}/${this.quizCode}/${this.questionIdArray[(this.count - 1)]}/${this.quizPlaySubmissionId}/${this.userSelectedOption}/true/${this.question.domain}`;
        this.postUserResponseService.postUserResponse(postUserSelectedOption).subscribe(evaluatedResult => {
            this.evaluatedResult = evaluatedResult;
            console.log("evaluating last question", this.evaluatedResult);
            this.endTimmer = true;
            // this.router.navigate(['/result', this.quizPlaySubmissionId]);
        });
    }
    startTimer() {
        this.interval = setInterval(() => {
            if (this.quizDuration > 1 && this.endTimmer == false) {
                this.quizDuration--;
            }
            else if (this.quizDuration == 1 && this.endTimmer == false && this.userSelectedOption != null) {
                this.quizDuration--;
                this.endQuiz();
            }
            else {
                clearInterval(this.interval);
                this.router.navigate(['/result', this.quizPlaySubmissionId]);
            }
        }, 1000);
    }
    endQuiz() {
        this.lastQuestion == true;
        const postUserSelectedOption = `quizplay/quizsubmissions/${this.psid}/${this.quizCode}/${this.questionIdArray[(this.count - 1)]}/${this.quizPlaySubmissionId}/${this.userSelectedOption}/true/${this.question.domain}`;
        this.postUserResponseService.postUserResponse(postUserSelectedOption).subscribe(evaluatedResult => {
            this.evaluatedResult = evaluatedResult;
            console.log("evaluating last question", this.evaluatedResult);
            this.endTimmer = true;
            // this.router.navigate(['/result', this.quizPlaySubmissionId]);
        });
    }
};
TestWindowComponent.ctorParameters = () => [
    { type: src_app_quiz_lobby_quiz_lobby_service__WEBPACK_IMPORTED_MODULE_2__["QuizLobbyService"] },
    { type: src_app_test_window_fetch_question_service__WEBPACK_IMPORTED_MODULE_3__["FetchQuestionService"] },
    { type: src_app_test_window_post_user_response_service__WEBPACK_IMPORTED_MODULE_5__["PostUserResponseService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
];
TestWindowComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-test-window',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./test-window.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/test-window/test-window.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./test-window.component.css */ "./src/app/test-window/test-window.component.css")).default]
    })
], TestWindowComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\34591\Desktop\San-git\santander-induction\santander-induction-web\SantanderWebApp\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map