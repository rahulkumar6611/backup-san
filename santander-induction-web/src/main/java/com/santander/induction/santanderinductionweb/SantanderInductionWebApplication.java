package com.santander.induction.santanderinductionweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SantanderInductionWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SantanderInductionWebApplication.class, args);
	}

}
