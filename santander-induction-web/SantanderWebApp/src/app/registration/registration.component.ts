import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material';
import { LoginComponent } from 'src/app/login/login.component';
import { LandingPageComponent } from 'src/app/landing-page/landing-page.component';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  msg : string;
  hide = true;
  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }
  constructor(private router:Router,private dialogRef:MatDialogRef<LandingPageComponent>) { }

  ngOnInit() {
  }
 submit(){
   this.msg = "Successfully Registered, Kindly login to continue.";
   this.router.navigateByUrl('');
   this.dialogRef.close();
 }
}
