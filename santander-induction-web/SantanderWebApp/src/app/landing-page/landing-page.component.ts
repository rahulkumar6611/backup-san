import { Component, OnInit } from '@angular/core';
import { LoginComponent } from 'src/app/login/login.component';
import { MatDialog } from '@angular/material';
import { RegistrationComponent } from 'src/app/registration/registration.component';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor(public dialog:MatDialog) { }

  ngOnInit() {
  }

  openLoginDialog(){
 this.dialog.open(LoginComponent,{
   width:'600px',
   height:'500px'
 });
}

openRegisterDialog(){
  this.dialog.open(RegistrationComponent,{
    width:'600px',
    height:'500px'
  });
 }
}
