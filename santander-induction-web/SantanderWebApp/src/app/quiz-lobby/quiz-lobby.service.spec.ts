import { TestBed } from '@angular/core/testing';

import { QuizLobbyService } from './quiz-lobby.service';

describe('QuizLobbyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuizLobbyService = TestBed.get(QuizLobbyService);
    expect(service).toBeTruthy();
  });
});
