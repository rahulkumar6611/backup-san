import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';


@Injectable({
  providedIn: 'root'
})
export class QuizLobbyService {

  constructor(private httpClient: HttpClient) { }


  //To fetch the info of any particular quiz for quiz lobby..
  getQuizLobbyInfo(url: string):Observable<any> {
    return this.httpClient.get<any>(url);
  }

  startQuiz(url:string):Observable<any> {
    return this.httpClient.get<any>(url);
  }

  //to pass question Array to the test window componet

  private listener = new BehaviorSubject([]);
  sendQuestionArray(questionIdArray : any) {
    this.listener.next(questionIdArray);
  }
  getQuestionArray(): Observable<any> {
    return this.listener;
  }


  private timeListener = new BehaviorSubject('');
  sendQuizDuration(quizDuration : any) {
    this.timeListener.next(quizDuration);
  }
  getQuizDuration(): Observable<any> {
    return this.timeListener;
  }

  private quizNameListener = new BehaviorSubject('');
  sendQuizName(quizName : any) {
    this.quizNameListener.next(quizName);
  }
  getQuizName(): Observable<any> {
    return this.quizNameListener;
  }


  private quizPlaySubmissionIdListener = new BehaviorSubject('');
  sendQuizPlaySubmissionId(quizSubmissionId : any) {
    console.log("submissionId in service send-->", quizSubmissionId);
    this.quizPlaySubmissionIdListener.next(quizSubmissionId);
  }
  getQuizPlaySubmissionId(): Observable<any> {
    console.log("submission id in service get", this.quizPlaySubmissionIdListener);
    return this.quizPlaySubmissionIdListener;
  }


}
