import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { QuizLobbyService } from 'src/app/quiz-lobby/quiz-lobby.service';
import { PostUserResponseService } from 'src/app/test-window/post-user-response.service';
import { FetchQuestionService } from 'src/app/test-window/fetch-question.service';


@Component({
  selector: 'app-quiz-lobby',
  templateUrl: './quiz-lobby.component.html',
  styleUrls: ['./quiz-lobby.component.css']
})
export class QuizLobbyComponent implements OnInit {

  public id;
  public quiz;
  public quizCode : any ;
  public psid ;
  public questionIdArray : any = [] ;
  public tempQuestionIdArray : any = [];
  public tempQuizSubmissionData;
  public quizSubmissionId;
  public userMail;
  public questionIds;

  @Output() sendQuestionArray: EventEmitter<any> = new EventEmitter();
 
  @Output() sendQuizDuration: EventEmitter<any> = new EventEmitter();

  @Output() sendQuizName: EventEmitter<any> = new EventEmitter();

  @Output() sendQuizPlaySubmissionId: EventEmitter<any> = new EventEmitter();

  constructor(
    private quizLobbyService: QuizLobbyService,
    private route: ActivatedRoute,
    private router: Router,
    private postUserResponse : PostUserResponseService,
    private fetchUserInfoService: FetchQuestionService ) { }

  ngOnInit() {
    // const id = this.route.snapshot.paramMap.get('id');
    // this.quizCode = id;
    //*--------Below *ParamMap observable* is the substitute of SNAPSHOT approach.
    this.route.paramMap.subscribe((params: ParamMap) =>{
      let id = params.get('id');
      this.quizCode = id;
    });
    const url = `quiz/quizzes/${this.quizCode}`;
    this.quizLobbyService.getQuizLobbyInfo(url).subscribe(res => {
      this.quiz = res;
      console.log("quiz fetched for quiz lobby-->", this.quiz);
      this.questionIds = this.quiz.questionId;
      console.log("Question Ids in quizlobby--", this.questionIds);
      this.shuffleQuestions(this.questionIds);
      // console.log("shuffled questionArray-->", this.shuffleQuestions(this.questionIds));
    })


    const loadUserUrl = `user/userprofile`;
    this.fetchUserInfoService.loadUserProfile(loadUserUrl).subscribe(res => {
      this.psid = res.psid;
      this.userMail = res.email;
      console.log("user psid-------->>>>", this.psid);
      console.log("user-mail---> ", this.userMail);
    })

  }


  shuffleQuestions(tempQuestionIdArray) {
    var currentIndex = tempQuestionIdArray.length, temporaryValue, randomIndex;
    //while there is element to shuffle..
    while(0 != currentIndex) {
      //Pick a random element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      //And swap it with the current element..
      temporaryValue =tempQuestionIdArray[currentIndex];
      tempQuestionIdArray[currentIndex] = tempQuestionIdArray[randomIndex];
      tempQuestionIdArray[randomIndex] = temporaryValue;
    }   
    this.questionIdArray = tempQuestionIdArray;
    console.log("in ShuffleMethod====>", this.questionIdArray);
  }


  async startQuiz() {
    //creating a quiz submission when user clicks on Play Quiz button.
    const postSubmissionUrl = `quizplay/quizsubmissions/${this.psid}/${this.quizCode}`;
    await this.postUserResponse.createUserSubmission(postSubmissionUrl).subscribe( res => { 
      this.tempQuizSubmissionData = res;
      this.quizSubmissionId = this.tempQuizSubmissionData.result;
      this.quizLobbyService.sendQuizPlaySubmissionId(this.quizSubmissionId);
      console.log("quiz Submission details--:::--> ", this.quizSubmissionId);
      console.log("IMPORTANT");
    })

    //passing the qestionId's, quizName and the duration of quiz to the testWindow component.
    await console.log("first await...++++++++>>>>>>>>>>>>>");
    await this.quizLobbyService.sendQuestionArray(this.questionIdArray);
    await this.quizLobbyService.sendQuizDuration(this.quiz.maxDurationMinutes);
    await this.quizLobbyService.sendQuizName(this.quiz.quizName); 
    console.log("on start click (quizlobby.ts) -- >", this.questionIdArray);
    this.router.navigate(['/testwindow',this.quizCode]);
  }

  onCancel() {
    this.router.navigate(['/dashboard']);
  }

}
