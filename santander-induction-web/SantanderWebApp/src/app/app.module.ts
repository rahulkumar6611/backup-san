import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedComponentModule } from './shared-component/shared-component.module'
import { HttpClientModule, HTTP_INTERCEPTORS, HttpHeaders } from '@angular/common/http';
import { DemoMaterialModule} from './material-module';
import { FormsModule} from '@angular/forms';
import { ReactiveFormsModule} from '@angular/forms';

// import { QuizLobbyComponent } from './quiz-lobby/quiz-lobby.component';
// import { QuizPlayComponent } from './quiz-play/quiz-play.component';
// import {
//   MatButtonModule,
//   MatCardModule,
//   MatTabsModule
// } from "@angular/material";
import { TestWindowComponent } from './test-window/test-window.component';
import { LoginComponent } from './login/login.component';
import { ResultComponent } from './result/result.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SanCarouselComponent } from './san-carousel/san-carousel.component';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTabsModule } from '@angular/material/tabs';
import { LandingPageComponent } from './landing-page/landing-page.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegistrationComponent } from './registration/registration.component';
import { AuthInterceptorService } from './auth-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    TestWindowComponent,
    LoginComponent,
    ResultComponent,
    NavBarComponent,
    SanCarouselComponent,
    LandingPageComponent,
    RegistrationComponent,
    // QuizLobbyComponent,
    // QuizPlayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedComponentModule,
    HttpClientModule,
    DemoMaterialModule,
    NgbCarouselModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  exports: [DemoMaterialModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
