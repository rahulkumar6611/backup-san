import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SanCarouselComponent } from './san-carousel.component';

describe('SanCarouselComponent', () => {
  let component: SanCarouselComponent;
  let fixture: ComponentFixture<SanCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SanCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SanCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
