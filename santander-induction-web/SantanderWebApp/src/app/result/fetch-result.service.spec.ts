import { TestBed } from '@angular/core/testing';

import { FetchResultService } from './fetch-result.service';

describe('FetchResultService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchResultService = TestBed.get(FetchResultService);
    expect(service).toBeTruthy();
  });
});
