import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { FetchResultService } from 'src/app/result/fetch-result.service';
import { FetchQuestionService } from 'src/app/test-window/fetch-question.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  public quizSubmissionId;
  public quizResult;
  public totalMarks;
  public psid;
  public userMail;


  constructor( private route: ActivatedRoute,
               private fetchResultService: FetchResultService,
               private fetchUserInfoService: FetchQuestionService ) { }

  ngOnInit() {

    console.log("NG_ONINIT in Result window.....::::::::::: ");

    this.route.paramMap.subscribe((params: ParamMap) =>{
      let id = params.get('id');
      this.quizSubmissionId = id;
      console.log("in result view-->", this.quizSubmissionId);

    });

    console.log("QUIZ-SUbmission id in result view ouside subscribe-->", this.quizSubmissionId);

    const url1= `quizplay/quizsubmissions/${this.quizSubmissionId}`;
    this.fetchResultService.getQuizResult(url1).subscribe( res => {
      this.quizResult = res;
      this.quizSubmissionId = this.quizResult.submissionId;
      this.totalMarks = this.quizResult.totalPointsScored;
    })


    const loadUserUrl = `user/userprofile`;
    this.fetchUserInfoService.loadUserProfile(loadUserUrl).subscribe(res => {
      this.psid = res.psid;
      this.userMail = res.email;
      console.log("user psid>>>>", this.psid);
      console.log("user-mail---> ", this.userMail);
    })


  }


}
