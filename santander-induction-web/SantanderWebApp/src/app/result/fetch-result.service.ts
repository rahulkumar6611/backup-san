import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class FetchResultService {

  constructor( private httpClient: HttpClient) { }


  getQuizResult(url: string):Observable<any> {
    return this.httpClient.get<any>(url);
  }
}
