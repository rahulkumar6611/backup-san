import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { FormGroup } from '@angular/forms';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserLoginService {

  constructor(private httpClient: HttpClient) { }


  validateUser(url: string): Observable<any> {
    return this.httpClient.get<any>(url);
  }

  checkClientData(user: FormGroup): Observable<any> {
    return this.httpClient.post(`user/login`, user);
  }

}
