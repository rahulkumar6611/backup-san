import { Component, OnInit ,Input, Output, EventEmitter }from '@angular/core';
import { FormControl, FormGroup} from '@angular/forms';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material';
import { UserLoginService } from 'src/app/login/user-login.service';
import * as CryptoJS from 'crypto-js';
import { HttpHeaders } from '@angular/common/http';

export interface ResponseModel {
  statusCode: number;
  message: string;
 }

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    psid: new FormControl(''),
    password: new FormControl(''),
  });

  // hide =true;
  // public data1;
  // public userPsid;
  // public password;

  constructor(private router:Router,
    private dialogRef:MatDialogRef<DashboardComponent>,
    private loginService : UserLoginService) { }

  ngOnInit() {
    console.log("login-component inside ngonit......");
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.log(this.loginForm.value);
    this.loginService.checkClientData(this.loginForm.value).subscribe((data) => {
      console.log('login-component====>');
      const response = data;
      console.log('statusCODE??????????????????????????' + response.statusCode);
      if (response.statusCode === 200 ) {
        this.router.navigate(['/dashboard']);
      } else {
        this.router.navigate(['']);
      }
      this.dialogRef.close();
    });


  }


  // onLogin() {
  //   var pw = "santander_key_quiz_pass";
  //   var pt = this.password;
    // var encrypted = CryptoJS.AES.encrypt(pt, pw);
    // var encodedPass = encodeURIComponent(encrypted.toString());
    // let httpOptions = {
    //   headers: new HttpHeaders({
    //     'psid': this.userPsid,
    //     'password': encodedPass,
    //   })
    // };
    // // let headers = new HttpHeaders();
    // // headers = headers.set('password', encodedPass);

    // console.log("::::----->"+encrypted.toString());
    // console.log("encoded pass--->", encodedPass);
    // encrypted.salt.toString();
    // encrypted.ciphertext.toString();
    // console.log("cypher key---> ",encrypted.ciphertext.toString() );

  //   console.log("login-comp......");
  //   const url = `user/login?psid=${this.userPsid}&password=${this.password}`;
  //   this.loginService.validateUser(url).subscribe( res => {
  //     this.data1 = res;
  //     console.log("login-component====>", this.data1);
  //     this.dialogRef.close();
  //   });

  //     // this.router.navigateByUrl('dashboard');
       
  // }


  // @Input() error: string | null;

  // @Output() submitEM = new EventEmitter();


}
