import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() quiz;
  @Input() componentName;
  fav = true;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  addToFavourite($quizCode){
  }
  
  onSelect(){
    this.router.navigate(['/quizlobby', this.quiz.quizCode]);
  }

}
