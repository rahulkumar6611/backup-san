import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { AppRoutingModule } from '../app-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from "@angular/material";
import { MatDividerModule } from "@angular/material";
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [CardComponent, 
    HeaderComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FlexLayoutModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,  
    MatChipsModule,
    MatDividerModule,
    MatTooltipModule,
    BrowserAnimationsModule      
  ],
  exports: [
    CardComponent,
    MatIconModule,
    MatButtonModule,  
    MatChipsModule,
    FlexLayoutModule,
    MatCardModule,
    MatDividerModule,
    MatTooltipModule,
    BrowserAnimationsModule,
    HeaderComponent
  ]
})
export class SharedComponentModule { }
