import { Component, OnInit, Input } from '@angular/core';
import { QuizLobbyService } from 'src/app/quiz-lobby/quiz-lobby.service';
import { FetchQuestionService } from 'src/app/test-window/fetch-question.service';
import { timer } from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PostUserResponseService } from 'src/app/test-window/post-user-response.service';


@Component({
  selector: 'app-test-window',
  templateUrl: './test-window.component.html',
  styleUrls: ['./test-window.component.css']
})
export class TestWindowComponent implements OnInit {
   
  // favoriteSeason: string;
  // seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];
  public questionIdArray: any=[];
  public question ;
  // questionArray1: any = [];
  public firstQuestionId;
  public totalNoOfQuestions;
  public count =1;
  public quizDuration;
  public quizName;
  public interval;
  public userSelectedOption : String;
  public lastQuestion : boolean = false;
  public psid ;
  public userMail;
  public quizCode;
  public quizPlaySubmissionId;
  public evaluatedResult;
  public endTimmer = false;


  constructor( private quizLobbyService: QuizLobbyService,
               private fetchQuestionArrayService: FetchQuestionService,
               private postUserResponseService: PostUserResponseService, 
               private router: Router,
               private route: ActivatedRoute ) { }

  // constructor(){}
  ngOnInit() {

    this.quizLobbyService.getQuestionArray().subscribe(data => {
      this.questionIdArray = data;
      this.totalNoOfQuestions =  this.questionIdArray.length;
      console.log("total number of questions in the system " , this.totalNoOfQuestions);
      this.firstQuestionId = this.questionIdArray[0];
      console.log("Question array in test window...", this.questionIdArray );
      console.log("first questionId:---> ", this.questionIdArray[0])
    });

    this.quizLobbyService.getQuizDuration().subscribe(quizDuration => {
      this.quizDuration = quizDuration;
      console.log("Quiz Duration ----> ", this.quizDuration);
    })

    this.quizLobbyService.getQuizName().subscribe(quizname => {
      this.quizName = quizname;
    })

    this.quizLobbyService.getQuizPlaySubmissionId().subscribe(submissionId => {
      this.quizPlaySubmissionId = submissionId;
      console.log("submission id in test window---:::-->", this.quizPlaySubmissionId);
    })

    this.route.paramMap.subscribe((params: ParamMap) =>{
      let id = params.get('quizCode');
      this.quizCode = id;
      console.log("QUIZCODE____>", this.quizCode);
    });

    const urlfetchQuestionById = `ques/questions/${this.firstQuestionId}`;
    this.fetchQuestionArrayService.fetchQuestionById(urlfetchQuestionById).subscribe(res => {
      this.question = res;
      console.log("first question fetched---->", this.question);
    })


    const loadUserUrl = `user/userprofile`;
    this.fetchQuestionArrayService.loadUserProfile(loadUserUrl).subscribe(res => {
      this.psid = res.psid;
      this.userMail = res.email;
      console.log("user psid>>>>", this.psid);
      console.log("user-mail---> ", this.userMail);
    })  

    this.startTimer();
  }


  onNextButtonClick() {
    // if(this.count < this.totalNoOfQuestions) {
    //   const urlfetchQuestionById = `santander-question-inventory/questions/${this.questionIdArray[this.count]}`;
    //   this.fetchQuestionArrayService.fetchQuestionById(urlfetchQuestionById).subscribe(res =>{
    //     this.question.push(res);
    //     console.log("Next question fetched---->", this.question);
    //     this.count ++;
    //   })
    // }
    if(this.count <= (this.totalNoOfQuestions - 1)) {
      const urlfetchQuestionById = `ques/questions/${this.questionIdArray[this.count]}`;
      this.fetchQuestionArrayService.fetchQuestionById(urlfetchQuestionById).subscribe(res => {
        this.question = res;
        console.log("Next question fetched---->", this.question);
        this.count ++;
      });

      console.log("domain is:>>>>>>>>>>>>>>>>>>>>>>", this.question.domain);
      //posting the user selected option with the (question Id - 1) to evaluate
        const postUserSelectedOption = `quizplay/quizsubmissions/${this.psid}/${this.quizCode}/${this.questionIdArray[(this.count -1)]}/${this.quizPlaySubmissionId}/${this.userSelectedOption}/${this.lastQuestion}`;
        this.postUserResponseService.postUserResponse(postUserSelectedOption).subscribe(evaluatedResult =>{
          this.evaluatedResult = evaluatedResult;
        })
      console.log("user selected option--> ", this.userSelectedOption);

    }else if(this.count == this.totalNoOfQuestions) {
      this.evaluatingLastQuestion();
    }
  }


  evaluatingLastQuestion() {
    // this.lastQuestion == true;
      const postUserSelectedOption = `quizplay/quizsubmissions/${this.psid}/${this.quizCode}/${this.questionIdArray[(this.count -1)]}/${this.quizPlaySubmissionId}/${this.userSelectedOption}/true`;

        this.postUserResponseService.postUserResponse(postUserSelectedOption).subscribe(evaluatedResult =>{
          this.evaluatedResult = evaluatedResult;
          console.log("evaluating last question",this.evaluatedResult);
          this.endTimmer = true;
          // this.router.navigate(['/result', this.quizPlaySubmissionId]);
        })
  }


  startTimer() {
    this.interval = setInterval(() => {
      if(this.quizDuration > 1 && this.endTimmer == false) {
        this.quizDuration--;
      } else if ( this.quizDuration == 1 && this.endTimmer == false && this.userSelectedOption != null) {
        this.quizDuration--;
        this.endQuiz();
      }
      else {
        clearInterval(this.interval);
        this.router.navigate(['/result', this.quizPlaySubmissionId]);
      }
    },1000)
  }


  endQuiz(){
    this.lastQuestion == true;
      const postUserSelectedOption = `quizplay/quizsubmissions/${this.psid}/${this.quizCode}/${this.questionIdArray[(this.count -1)]}/${this.quizPlaySubmissionId}/${this.userSelectedOption}/true`;
        this.postUserResponseService.postUserResponse(postUserSelectedOption).subscribe(evaluatedResult =>{
          this.evaluatedResult = evaluatedResult;
          console.log("evaluating last question",this.evaluatedResult);
          this.endTimmer = true;
          // this.router.navigate(['/result', this.quizPlaySubmissionId]);
        })
  }


}
