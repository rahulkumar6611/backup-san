import { TestBed } from '@angular/core/testing';

import { PostUserResponseService } from './post-user-response.service';

describe('PostUserResponseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostUserResponseService = TestBed.get(PostUserResponseService);
    expect(service).toBeTruthy();
  });
});
