import { TestBed } from '@angular/core/testing';

import { FetchQuestionService } from './fetch-question.service';

describe('FetchQuestionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchQuestionService = TestBed.get(FetchQuestionService);
    expect(service).toBeTruthy();
  });
});
