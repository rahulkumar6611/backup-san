import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FetchQuestionService {

  constructor(private httpClient: HttpClient) { }

  
  fetchQuestionById(url: string):Observable<any> {
    return this.httpClient.get<any>(url);
  }


  loadUserProfile(url: string): Observable<any> {
    return this.httpClient.get<any>(url);
  }


}
