import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PostUserResponseService {

  constructor( private httpClient: HttpClient ) { }


  createUserSubmission(url: string): Observable<void>{
    return this.httpClient.post<any>(url, null);
  }

  postUserResponse(url: string): Observable<void>{
    return this.httpClient.post<any>(url, null);
  }



}
