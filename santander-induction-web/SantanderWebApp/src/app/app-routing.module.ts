import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import { QuizLobbyComponent } from 'src/app/quiz-lobby/quiz-lobby.component';
import { TestWindowComponent } from 'src/app/test-window/test-window.component';
import { ResultComponent } from 'src/app/result/result.component';
import { LoginComponent } from 'src/app/login/login.component';
import { LandingPageComponent } from 'src/app/landing-page/landing-page.component';
import { RegistrationComponent } from 'src/app/registration/registration.component';


const routes: Routes = [
  { path: '' , component: LandingPageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'quizlobby/:id', component: QuizLobbyComponent },
  { path: 'testwindow/:quizCode', component: TestWindowComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'result/:id', component: ResultComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes , { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  DashboardComponent,
  QuizLobbyComponent,
  TestWindowComponent,
  ResultComponent,
  LoginComponent,
  LandingPageComponent,
  RegistrationComponent
]