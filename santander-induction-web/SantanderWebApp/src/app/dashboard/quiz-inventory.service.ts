import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class QuizInventoryService {

  // Url = 'http://localhost:8060/quizzes';
  Url1 = 'quiz/quizzes';

  constructor(private httpClient: HttpClient) { }


  getQuizzes(): Observable<any>{
    return this.httpClient.get<any>(this.Url1);
  }


}
