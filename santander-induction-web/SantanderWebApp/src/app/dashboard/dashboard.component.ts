import { Component, OnInit } from '@angular/core';
import { QuizInventoryService } from 'src/app/dashboard/quiz-inventory.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  dashboard = 'dashboard';
  listOfAllQuizzes;

  constructor(
    private quizInventoryService : QuizInventoryService
   ) { }

  ngOnInit() {

    this.getQuizzes();
  }

  getQuizzes(){
    this.quizInventoryService.getQuizzes().subscribe(data => {
      this.listOfAllQuizzes = data;
      console.log("quizzes in the system::-->",this.listOfAllQuizzes);
    })
  }

//  public listOfAllQuizzes = [
//       {
//         "quizCode": "q1",
//         "quizName": "quizName1",
//         "description": "lasdsah djkhaskjdh ajsdhasd has dha hdajshd as ",
//         "createDate": "2019-12-25T19:52:34.858+0000",
//         "maxDurationMinutes": 10,
//         "maxScore": 20.0,
//         "openTS": "2019-12-25T19:52:34.858+0000",
//         "closeTS": "2019-12-25T19:52:34.858+0000",
//         "maxAttempts": 0,
//         "concepts": ["java","xml","spring","php"],
//         "questionId": [
//             "9137ec64-bde4-4c30-b8fa-2533b073170d",
//             "48ff5031-896b-49c5-8b9f-e573366433fc"
//         ]
//     }
//   ]


}
