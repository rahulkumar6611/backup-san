package com.santander.induction.santanderquizinventory.service;

import java.util.List;

import com.santander.induction.santanderquizinventory.model.Quiz;

public interface IQuizService {
	
	List<Quiz> getAllQuizzes();
	Quiz createQuiz(Quiz quiz);
	Quiz findByQuizCode(String quizCode);
	Quiz modifyQuiz(String quizCode, Quiz quiz);
}
