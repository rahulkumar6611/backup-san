package com.santander.induction.santanderquizinventory.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.santander.induction.santanderquizinventory.model.Quiz;
import com.santander.induction.santanderquizinventory.repo.IQuizRepository;

@Service
public class QuizServiceImpl implements IQuizService {

	@Autowired
	private IQuizRepository quizRepo;
	
	
	@Override
	public List<Quiz> getAllQuizzes() {
		List<Quiz> listOfQuizzes = quizRepo.findAll();
		return listOfQuizzes;
	}

	@Override
	public Quiz createQuiz(Quiz quiz) {
		Quiz newQuiz = quizRepo.save(quiz);
		return newQuiz;
	}

	@Override
	public Quiz findByQuizCode(String quizCode) {
		return quizRepo.findByQuizCode(quizCode);
	}

	@Override
	public Quiz modifyQuiz(String quizCode, Quiz quiz) {
		Quiz quiz1 = quizRepo.findByQuizCode(quizCode);
		if (quiz.getQuestionId() != null) {
			quiz1.setQuestionId(quiz.getQuestionId());
		}
		Quiz quiz12= quizRepo.save(quiz1);
		return quiz12;
	}
	

}
