package com.santander.induction.santanderquizinventory.model;


import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class Quiz {

	@Id
	private String quizCode;
	private String quizName;
	private String description;
	@CreatedDate
	private Date createDate=new Date();
	private int maxDurationMinutes;
	// sum of marks of all the questions.
	private float maxScore;
	private Date openTS=new Date();
	private Date closeTS=new Date();
	private int maxAttempts;
	private ArrayList<String> concepts= new ArrayList<>();
	private ArrayList<UUID> questionId = new ArrayList<>();
//	private boolean quuizStatus = true;
	
	public Quiz() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Quiz(String quizCode, String quizName, String description, Date createDate, int maxDurationMinutes,
			float maxScore, Date openTS, Date closeTS, int maxAttempts, ArrayList<String> concepts,
			ArrayList<UUID> questionId) {
		super();
		this.quizCode = quizCode;
		this.quizName = quizName;
		this.description = description;
		this.createDate = createDate;
		this.maxDurationMinutes = maxDurationMinutes;
		this.maxScore = maxScore;
		this.openTS = openTS;
		this.closeTS = closeTS;
		this.maxAttempts = maxAttempts;
		this.concepts = concepts;
		this.questionId = questionId;
	}

	public String getQuizCode() {
		return quizCode;
	}

	public void setQuizCode(String quizCode) {
		this.quizCode = quizCode;
	}

	public String getQuizName() {
		return quizName;
	}

	public void setQuizName(String quizName) {
		this.quizName = quizName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public int getMaxDurationMinutes() {
		return maxDurationMinutes;
	}

	public void setMaxDurationMinutes(int maxDurationMinutes) {
		this.maxDurationMinutes = maxDurationMinutes;
	}

	public float getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(float maxScore) {
		this.maxScore = maxScore;
	}

	public Date getOpenTS() {
		return openTS;
	}

	public void setOpenTS(Date openTS) {
		this.openTS = openTS;
	}

	public Date getCloseTS() {
		return closeTS;
	}

	public void setCloseTS(Date closeTS) {
		this.closeTS = closeTS;
	}

	public int getMaxAttempts() {
		return maxAttempts;
	}

	public void setMaxAttempts(int maxAttempts) {
		this.maxAttempts = maxAttempts;
	}

	public ArrayList<String> getConcepts() {
		return concepts;
	}

	public void setConcepts(ArrayList<String> concepts) {
		this.concepts = concepts;
	}

	public ArrayList<UUID> getQuestionId() {
		return questionId;
	}

	public void setQuestionId(ArrayList<UUID> questionId) {
		this.questionId = questionId;
	}

	
	
}
