package com.santander.induction.santanderquizinventory.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.santander.induction.santanderquizinventory.model.Quiz;

@Repository
public interface IQuizRepository extends MongoRepository<Quiz,Integer>{

	Quiz findByQuizCode(String quizCode);
}
