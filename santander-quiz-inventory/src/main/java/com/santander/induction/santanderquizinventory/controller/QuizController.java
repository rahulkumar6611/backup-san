package com.santander.induction.santanderquizinventory.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.santander.induction.santanderquizinventory.model.Quiz;
import com.santander.induction.santanderquizinventory.service.QuizServiceImpl;

@RestController
public class QuizController {

	@Autowired
	private QuizServiceImpl quizService;
	
	
	@GetMapping("/quizzes")
	public List<Quiz> getAllQuiz(){
		List<Quiz> listOfQuizzes = quizService.getAllQuizzes();
		return listOfQuizzes;		
	}
	
	@PostMapping("/quizzes")
	private Quiz createQuiz(@RequestBody Quiz quiz) {
		Quiz newQuiz = quizService.createQuiz(quiz);
		return newQuiz;
	}
	
	@GetMapping("/quizzes/{quizCode}")
	public Quiz findQuizByQuizCode(@PathVariable("quizCode") String quizCode) {
		Quiz quiz = quizService.findByQuizCode(quizCode);
		return quiz;
	}
	
	@PatchMapping("/quizzes/{quizCode}")
	public Quiz modifyQuiz(@PathVariable("quizCode") String quizCode, @RequestBody Quiz quiz) {
		return quizService.modifyQuiz(quizCode, quiz);
	}
}
