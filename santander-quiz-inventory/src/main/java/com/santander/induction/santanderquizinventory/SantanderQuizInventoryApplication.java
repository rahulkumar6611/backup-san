package com.santander.induction.santanderquizinventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SantanderQuizInventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SantanderQuizInventoryApplication.class, args);
	}

}
